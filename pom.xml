<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <groupId>hr.tvz.npupj</groupId>
    <artifactId>minireferada</artifactId>
    <version>FINAL</version>
    <packaging>jar</packaging>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${v.maven-compiler-plugin}</version>
                    <configuration>
                        <source>${v.java}</source>
                        <target>${v.java}</target>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>${v.maven-jar-plugin}</version>
                    <configuration>
                        <archive>
                            <manifestEntries>
                                <specification-title>${project.name}</specification-title>
                                <specification-version>${project.version}</specification-version>
                                <specification-vendor>${project.organization.name}</specification-vendor>
                                <implementation-title>${project.name}</implementation-title>
                                <implementation-version>${project.version}</implementation-version>
                                <implementation-vendor-id>${project.groupId}</implementation-vendor-id>
                                <implementation-vendor>${project.organization.name}</implementation-vendor>
                            </manifestEntries>
                        </archive>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${v.maven-surefire-plugin}</version>
                <configuration>
                    <includes>
                        <include>**/*.class</include>
                    </includes>
                    <excludes>
                        <exclude>**/view/*.java</exclude>
                    </excludes>
                    <excludedGroups>hr.tvz.npupj.minireferada.integration.AbstractIntegrationTest</excludedGroups>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>2.12</version>
                <dependencies>
                    <dependency>
                        <groupId>org.apache.maven.surefire</groupId>
                        <artifactId>surefire-junit47</artifactId>
                        <version>${v.maven-failsafe-plugin}</version>
                    </dependency>
                </dependencies>
                <configuration>
                    <groups>hr.tvz.npupj.minireferada.integration.AbstractIntegrationTest</groups>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>integration-test</goal>
                        </goals>
                        <configuration>
                            <includes>
                                <include>**/*.class</include>
                            </includes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <v.java>1.8</v.java>

        <!-- COMPILE -->
        <v.apache.commons>3.3.2</v.apache.commons>
        <v.guava>18.0</v.guava>
        <v.h2>1.4.187</v.h2>
        <v.hibernate>4.3.5.Final</v.hibernate>
        <v.jadira.usertype>3.2.0.GA</v.jadira.usertype>
        <v.joda.time>2.6</v.joda.time>
        <v.joda.time.hibernate>1.3</v.joda.time.hibernate>
        <v.mysql.connector>5.1.34</v.mysql.connector>
        <v.spring>4.1.6.RELEASE</v.spring>
        <v.spring.data-jpa>1.7.1.RELEASE</v.spring.data-jpa>
        <v.spring.security>4.0.1.RELEASE</v.spring.security>

        <!-- LOGGING -->
        <v.janino>2.7.7</v.janino>
        <v.logback>1.1.2</v.logback>
        <v.slf4j>1.7.10</v.slf4j>

        <!-- TEST -->
        <v.dbunit>2.4.9</v.dbunit>
        <v.junit>4.11</v.junit>
        <v.mockito>1.10.19</v.mockito>
        <v.testfx>4.0.1-alpha</v.testfx>

        <!-- PLUGINS -->
        <v.maven-compiler-plugin>3.2</v.maven-compiler-plugin>
        <v.maven-failsafe-plugin>2.12</v.maven-failsafe-plugin>
        <v.maven-jar-plugin>2.5</v.maven-jar-plugin>
        <v.maven-surefire-plugin>2.7.2</v.maven-surefire-plugin>

        <!-- SONAR -->
        <v.jacoco-plugin>0.7.5.201505241946</v.jacoco-plugin>
        <coverage.reports.dir>${basedir}/target/coverage-reports</coverage.reports.dir>
        <sonar.jacoco.reportPath>${coverage.reports.dir}/jacoco-unit.exec</sonar.jacoco.reportPath>
        <sonar.jacoco.itReportPath>${coverage.reports.dir}/jacoco-it.exec</sonar.jacoco.itReportPath>
        <sonar.jacoco.jar>${basedir}/lib/jacocoagent.jar</sonar.jacoco.jar>
    </properties>


    <dependencies>
        <!-- COMPILE -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>${v.apache.commons}</version>
        </dependency>

        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>${v.guava}</version>
        </dependency>

        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <version>${v.h2}</version>
        </dependency>

        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
            <version>${v.joda.time}</version>
        </dependency>
        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time-hibernate</artifactId>
            <version>${v.joda.time.hibernate}</version>
        </dependency>

        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-core</artifactId>
            <version>${v.hibernate}</version>
        </dependency>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-entitymanager</artifactId>
            <version>${v.hibernate}</version>
            <exclusions>
                <exclusion>
                    <artifactId>jboss-transaction-api_1.2_spec</artifactId>
                    <groupId>org.jboss.spec.javax.transaction</groupId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>${v.mysql.connector}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
            <version>${v.spring}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${v.spring}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context-support</artifactId>
            <version>${v.spring}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>${v.spring}</version>
            <exclusions>
                <exclusion>
                    <artifactId>commons-logging</artifactId>
                    <groupId>commons-logging</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.springframework.data</groupId>
            <artifactId>spring-data-jpa</artifactId>
            <version>${v.spring.data-jpa}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-expression</artifactId>
            <version>${v.spring}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>${v.spring}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-orm</artifactId>
            <version>${v.spring}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-web</artifactId>
            <version>${v.spring.security}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-config</artifactId>
            <version>${v.spring.security}</version>
        </dependency>

        <!-- LOGGING -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${v.slf4j}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>log4j-over-slf4j</artifactId>
            <version>${v.slf4j}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>jcl-over-slf4j</artifactId>
            <version>${v.slf4j}</version>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-core</artifactId>
            <version>${v.logback}</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>${v.logback}</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>org.codehaus.janino</groupId>
            <artifactId>janino</artifactId>
            <version>${v.janino}</version>
            <scope>compile</scope>
        </dependency>

        <!-- TEST -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${v.junit}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>${v.spring}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.dbunit</groupId>
            <artifactId>dbunit</artifactId>
            <version>${v.dbunit}</version>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <artifactId>junit</artifactId>
                    <groupId>junit</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <version>${v.mockito}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.testfx</groupId>
            <artifactId>testfx-core</artifactId>
            <version>${v.testfx}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.testfx</groupId>
            <artifactId>testfx-junit</artifactId>
            <version>${v.testfx}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.jacoco</groupId>
            <artifactId>jacoco-maven-plugin</artifactId>
            <version>${v.jacoco-plugin}</version>
        </dependency>
        <dependency>
            <groupId>org.jacoco</groupId>
            <artifactId>org.jacoco.agent</artifactId>
            <version>${v.jacoco-plugin}</version>
        </dependency>
    </dependencies>

</project>