package hr.tvz.npupj.minireferada.component;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.Region;
import javafx.util.Callback;

import org.springframework.ui.ModelMap;

/**
 * Created by mjovanovic on 30/05/15.
 */
public class TableColumnButtonCell<T, S> implements Callback<TableColumn<T, S>, TableCell<T, S>> {

    private ButtonConfig config;
    private ModelMap dataMap;
    
    public TableColumnButtonCell(ButtonConfig config) {
    	this.config = config;
    	this.dataMap = new ModelMap();
    }
    
    public TableColumnButtonCell(ButtonConfig config, ModelMap map) {
        this.config = config;
        this.dataMap = map;
    }

    @Override
    public TableCell<T, S> call(TableColumn<T, S> param) {

        Button button = new Button();
        button.setMinWidth(config.width);
        button.setText(config.text);

        TableCell<T, S> tableCell = new TableCell<T, S>() {
            @Override
            protected void updateItem(S item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null) {
                    button.setOnAction((event) -> {
                    	dataMap.put("cellValue", item);
                        config.callback.onClick(event, dataMap);
                    });
                    setGraphic(button);
                } else {
                    setGraphic(null);
                }
            }
        };

        return tableCell;
    }

    public interface CellButtonCallback {
        void onClick(ActionEvent event, ModelMap modelMap);
    }

    public static class ButtonConfig {
        private String             text;
        private double             width;
        private CellButtonCallback callback;

        public ButtonConfig(CellButtonCallback callback) {
            this("Button", callback);
        }

        public ButtonConfig(String text, CellButtonCallback callback) {
            this(text, Region.USE_COMPUTED_SIZE, callback);
        }

        public ButtonConfig(String text, double width, CellButtonCallback callback) {
            this.text = text;
            this.width = width;
            this.callback = callback;
        }

        public void setCallback(CellButtonCallback callback) {
            this.callback = callback;
        }

        public void setWidth(double width) {
            this.width = width;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
}
