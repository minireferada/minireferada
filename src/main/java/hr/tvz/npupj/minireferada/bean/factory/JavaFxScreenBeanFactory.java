package hr.tvz.npupj.minireferada.bean.factory;

import hr.tvz.npupj.minireferada.config.FxmlScreen;
import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by mjovanovic on 19/05/15.
 */
@Component
public class JavaFxScreenBeanFactory implements ApplicationListener<ContextRefreshedEvent>, ApplicationContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(JavaFxScreenBeanFactory.class);

    private ApplicationContext  appCtx;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        ConfigurableListableBeanFactory beanFactory = ((ConfigurableApplicationContext) appCtx).getBeanFactory();
        BeanDefinitionRegistry registry = (BeanDefinitionRegistry) beanFactory;

        Map<String, AbstractScreenController> controllers = beanFactory.getBeansOfType(AbstractScreenController.class);
        for (Map.Entry<String, AbstractScreenController> entry : controllers.entrySet()) {

            String beanName = getBeanName(entry.getKey());
            FxmlScreen fxmlScreen = new FxmlScreen(entry.getValue());

            LOGGER.info("Creating screen [{}]", beanName);

            GenericBeanDefinition beanDef = new GenericBeanDefinition();
            beanDef.setLazyInit(false);
            beanDef.setAbstract(false);
            beanDef.setAutowireCandidate(true);
            beanDef.setScope(BeanDefinition.SCOPE_PROTOTYPE);
            beanDef.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_NAME);

            ConstructorArgumentValues cav = new ConstructorArgumentValues();
            cav.addIndexedArgumentValue(0, entry.getValue());
            beanDef.setConstructorArgumentValues(cav);

            beanFactory.registerSingleton(beanName, fxmlScreen);
            beanDef.setBeanClass(fxmlScreen.getClass());

            registry.registerBeanDefinition(beanName, beanDef);

        }
    }

    private String getBeanName(String controllerName) {
        return StringUtils.substringBefore(controllerName, "Controller") + "Screen";
    }

    @Override
    public void setApplicationContext(ApplicationContext appCtx) throws BeansException {
        this.appCtx = appCtx;
    }
}
