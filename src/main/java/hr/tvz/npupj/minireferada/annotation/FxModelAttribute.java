package hr.tvz.npupj.minireferada.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by mjovanovic on 26/05/15.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FxModelAttribute {

    /**
     * This annotation is for specific controller.
     * 
     * @return
     */
    Class<?> controller() default Void.class;

    /**
     * Targeting field type.
     * 
     * @return
     */
    Class<?> value() default Void.class;

}
