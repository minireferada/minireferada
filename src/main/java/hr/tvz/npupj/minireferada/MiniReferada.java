package hr.tvz.npupj.minireferada;

import javafx.application.Application;
import javafx.stage.Stage;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import hr.tvz.npupj.minireferada.config.FxmlScreen;
import hr.tvz.npupj.minireferada.config.SpringConfiguration;

/**
 * Created by mjovanovic on 19/05/15.
 */
public class MiniReferada extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage p_stagetage) throws Exception {
        AnnotationConfigApplicationContext appCtx = new AnnotationConfigApplicationContext();
        appCtx.getEnvironment().setActiveProfiles(SpringConfiguration.PROFILE_LOCAL_NAME);
        appCtx.register(SpringConfiguration.class);
        appCtx.refresh();

        FxmlScreen loginScreen = appCtx.getBean("loginScreen", FxmlScreen.class);
        loginScreen.show();
    }
}
