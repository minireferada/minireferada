package hr.tvz.npupj.minireferada.dao;

import hr.tvz.npupj.minireferada.model.Osoba;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Osoba, BigInteger>{
		
	public Osoba getPersonByName(String name);
}
