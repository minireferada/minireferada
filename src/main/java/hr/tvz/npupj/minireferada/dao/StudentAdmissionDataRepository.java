package hr.tvz.npupj.minireferada.dao;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.tvz.npupj.minireferada.model.StudentAdmission;

;

/**
 * Created by mjovanovic on 29/05/15.
 */
public interface StudentAdmissionDataRepository extends JpaRepository<StudentAdmission, BigInteger> {

	StudentAdmission findById(BigInteger id); 
    
}
