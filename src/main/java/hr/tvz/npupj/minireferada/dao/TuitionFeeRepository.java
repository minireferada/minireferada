package hr.tvz.npupj.minireferada.dao;

import hr.tvz.npupj.minireferada.model.Student;
import hr.tvz.npupj.minireferada.model.TuitionFee;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TuitionFeeRepository extends JpaRepository<TuitionFee, BigInteger> {

	public List<TuitionFee> getTuituionFessByStudent(Student student);
	
	@Query("SELECT -sum(t.feeAmount) FROM TuitionFee t WHERE t.student = :student AND t.status IN ('DODJELA', 'PREDANO')")
	BigInteger sumAmountByPerson(@Param("student") Student student);

	public List<TuitionFee> getTuitionFeesByStatus(String status);
	
}
