package hr.tvz.npupj.minireferada.dao;

import hr.tvz.npupj.minireferada.model.Kolegij;
import hr.tvz.npupj.minireferada.model.Semestar;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


public interface KolegijRepository extends JpaRepository<Kolegij, BigInteger> {

	public List<Kolegij> findKolegijBySemestar(Semestar semestar);
}
