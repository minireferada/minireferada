package hr.tvz.npupj.minireferada.dao;

import hr.tvz.npupj.minireferada.model.WorkersRoles;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkersRepository extends JpaRepository<WorkersRoles, BigInteger> {
	
	public List<WorkersRoles> findAll();

}
