package hr.tvz.npupj.minireferada.dao;

import hr.tvz.npupj.minireferada.model.Semestar;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SemestarRepository extends JpaRepository<Semestar, BigInteger> {

	public List<Semestar> findAll();
}
