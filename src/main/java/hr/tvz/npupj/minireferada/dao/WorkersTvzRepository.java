package hr.tvz.npupj.minireferada.dao;

import hr.tvz.npupj.minireferada.model.WorkersTvz;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkersTvzRepository extends JpaRepository<WorkersTvz, BigInteger> {
	
	public List<WorkersTvz> findAll();
	

	
}
