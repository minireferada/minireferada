package hr.tvz.npupj.minireferada.dao;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.tvz.npupj.minireferada.model.Osoba;
import hr.tvz.npupj.minireferada.model.Student;

;

/**
 * Created by mjovanovic on 29/05/15.
 */
public interface StudentDataRepository extends JpaRepository<Student, BigInteger> {

    Student findByJmbag(String jmbag);

	Student findByUser(Osoba user);
    
}
