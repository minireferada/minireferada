package hr.tvz.npupj.minireferada.dao;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.tvz.npupj.minireferada.model.Course;

/**
 * Created by mjovanovic on 29/05/15.
 */
public interface CourseRepository extends JpaRepository<Course, BigInteger> {
    Course findByName(String p_name);
}
