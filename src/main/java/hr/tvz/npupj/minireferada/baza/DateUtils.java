package hr.tvz.npupj.minireferada.baza;

import hr.tvz.npupj.minireferada.config.HibernateConfiguration;
import hr.tvz.npupj.minireferada.model.Kolegij;
import hr.tvz.npupj.minireferada.model.Semestar;

import java.util.ArrayList;

import org.hibernate.Session;

public class DateUtils {

	@SuppressWarnings("unchecked")
	public static ArrayList<Semestar> fetchSemestars() {
		ArrayList<Semestar> semestri = new ArrayList<>();
		Session s = HibernateConfiguration.openSession();
		s.beginTransaction();
		semestri = (ArrayList<Semestar>) s.createQuery("from Semestar")
				.list();
		s.getTransaction().commit();
		s.close();
		return semestri;
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList<Kolegij> fetchCourses(){
		ArrayList<Kolegij> kolegiji = new ArrayList<>();
		Session s=HibernateConfiguration.openSession();
		s.beginTransaction();
		kolegiji= (ArrayList<Kolegij>)s.createQuery("from Kolegij");
		return kolegiji;
	}
}
