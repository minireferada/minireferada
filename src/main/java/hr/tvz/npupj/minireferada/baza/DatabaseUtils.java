package hr.tvz.npupj.minireferada.baza;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import org.hibernate.Session;

import hr.tvz.npupj.minireferada.config.HibernateConfiguration;
import hr.tvz.npupj.minireferada.model.Kolegij;
import hr.tvz.npupj.minireferada.model.Semestar;

public class DatabaseUtils {
    private static final String DATABASE_FILE = "database.properties";

    public static void spremiPodatke(Kolegij kolegiji) {

        Session s = HibernateConfiguration.openSession();
        s.beginTransaction();
        s.save(kolegiji);
        s.getTransaction().commit();
        s.close();
    }

    public static void obrisiPodatke(Kolegij kolegiji) {

        Session s = HibernateConfiguration.openSession();
        s.beginTransaction();
        s.delete(kolegiji);
        s.getTransaction().commit();
        s.close();
    }

    public static void urediPodatke(Kolegij kolegiji) {

        Session s = HibernateConfiguration.openSession();
        s.beginTransaction();
        s.update(kolegiji);
        s.getTransaction().commit();
        s.close();
    }

    @SuppressWarnings("unchecked")
    public static ArrayList<Semestar> dohvatiSemestreKolegija() {
        ArrayList<Semestar> semestri = new ArrayList<Semestar>();
        Session s = HibernateConfiguration.openSession();
        s.beginTransaction();
        semestri = (ArrayList<Semestar>) s.createQuery("from Semestar").list();
        s.getTransaction().commit();
        s.close();
        return semestri;
    }

    public static ArrayList<Kolegij> dohvatiKolegije(String nazivsemestra) throws SQLException, IOException {

        String query = "select naziv,pravonapotpis,ocjena from kolegij natural join semestar" + " where nazivsemestra=?";

        PreparedStatement stmt = conncetToDatabase().prepareStatement(query);

        stmt.setString(1, nazivsemestra);
        ResultSet rs = stmt.executeQuery();

        ArrayList<Kolegij> kolegiji = new ArrayList<Kolegij>();
        while (rs.next()) {
            String naziv = rs.getString("naziv");
            String pravoNaPotpis = rs.getString("pravonapotpis");
            int ocjena = rs.getInt("ocjena");
            kolegiji.add(new Kolegij(naziv, pravoNaPotpis, ocjena));
        }

        stmt.getConnection().close();
        return kolegiji;
    }

    private static Connection conncetToDatabase() throws SQLException, IOException {

        Properties svojstva = new Properties();

        svojstva.load(new FileReader(DATABASE_FILE));

        String urlBazePodataka = svojstva.getProperty("bazaPodatakaUrl");
        String korisnickoIme = svojstva.getProperty("korisnickoIme");

        String lozinka = svojstva.getProperty("lozinka");

        Connection veza = DriverManager.getConnection(urlBazePodataka, korisnickoIme, lozinka);

        return veza;
    }

    public static ArrayList<String> dohvatiSemestre() throws SQLException, IOException {

        Statement stmt = conncetToDatabase().createStatement();

        ResultSet rs = stmt.executeQuery("select nazivsemestra from semestar order by 1");

        ArrayList<String> semestri = new ArrayList<String>();

        while (rs.next()) {
            String naziv = rs.getString("nazivsemestra");
            semestri.add(naziv);
        }

        stmt.getConnection().close();
        return semestri;
    }

}
