package hr.tvz.npupj.minireferada.model;

import hr.tvz.npupj.minireferada.util.DateUtils;

import java.math.BigDecimal;
import java.util.Date;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TuitionFeeView {
	
	private StringProperty tuitionType;
	private StringProperty dueDate;
	private DoubleProperty feeAmount;
	private StringProperty description;
	private DoubleProperty paidAmount;
	private StringProperty status;
	private IntegerProperty id;
	private StringProperty filepath;
	
	public TuitionFeeView(TuitionFee db) {
		this();
		setTuitionType(db.getTuitionType());
		setDueDate(db.getDueDate());
		setFeeAmount(db.getFeeAmount());
		setDescription(db.getDescription());
		setPaidAmount(db.getPaidAmount());
		setStatus(db.getStatus());
		setId(db.getId().intValue());
		setFilepath(db.getFilePath());
	}
	
	public TuitionFeeView() {
		this.tuitionType = new SimpleStringProperty();
		this.dueDate = new SimpleStringProperty();
		this.feeAmount = new SimpleDoubleProperty();
		this.description = new SimpleStringProperty();
		this.paidAmount = new SimpleDoubleProperty();
		this.status = new SimpleStringProperty();
		this.id = new SimpleIntegerProperty();
		this.filepath = new SimpleStringProperty();
	}

	public String getTuitionType() {
		return tuitionType.get();
	}
	public void setTuitionType(String tuitionType) {
		this.tuitionType.setValue(tuitionType);
	}
	public String getDueDate() {
		return dueDate.get();
	}	
	
	public void setDueDate(Date dueDate) {
		this.dueDate.setValue(DateUtils.formatUiDate(dueDate));
	}
	public Double getFeeAmount() {
		return feeAmount.get();
	}
	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount.set(feeAmount.doubleValue());
	}
	public String getDescription() {
		return description.get();
	}
	public void setDescription(String description) {
		this.description.set(description);
	}
	public Double getPaidAmount() {
		return paidAmount.get();
	}
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount.set(paidAmount.doubleValue());
	}
	public String getStatus() {
		return status.get();
	}
	public void setStatus(String status) {
		this.status.set(status);
	}

	public Integer getId() {
		return id.getValue();
	}

	public void setId(Integer id) {
		this.id.setValue(id);
	}

	public String getFilepath() {
		return filepath.getValue();
	}

	public void setFilepath(String filepath) {
		this.filepath.set(filepath);
	}

	public StringProperty getTuitionTypeProperty() {
		return tuitionType;
	}

	public StringProperty getDueDateProperty() {
		return dueDate;
	}

	public DoubleProperty getFeeAmountProperty() {
		return feeAmount;
	}

	public StringProperty getDescriptionProperty() {
		return description;
	}

	public DoubleProperty getPaidAmountProperty() {
		return paidAmount;
	}

	public StringProperty getStatusProperty() {
		return status;
	}

	public IntegerProperty getIdProperty() {
		return id;
	}

	public StringProperty getFilepathProperty() {
		return filepath;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((dueDate == null) ? 0 : dueDate.hashCode());
		result = prime * result
				+ ((feeAmount == null) ? 0 : feeAmount.hashCode());
		result = prime * result
				+ ((filepath == null) ? 0 : filepath.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((paidAmount == null) ? 0 : paidAmount.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result
				+ ((tuitionType == null) ? 0 : tuitionType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TuitionFeeView other = (TuitionFeeView) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.getValue().equals(other.description.getValue()))
			return false;
		if (dueDate == null) {
			if (other.dueDate != null)
				return false;
		} else if (!dueDate.getValue().equals(other.dueDate.getValue()))
			return false;
		if (feeAmount == null) {
			if (other.feeAmount != null)
				return false;
		} else if (!feeAmount.getValue().equals(other.feeAmount.getValue()))
			return false;
		if (filepath == null) {
			if (other.filepath != null)
				return false;
		} else if (!filepath.getValue().equals(other.filepath.getValue()))
			return false;
		if (paidAmount == null) {
			if (other.paidAmount != null)
				return false;
		} else if (!paidAmount.getValue().equals(other.paidAmount.getValue()))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.getValue().equals(other.status.getValue()))
			return false;
		if (tuitionType == null) {
			if (other.tuitionType != null)
				return false;
		} else if (!tuitionType.getValue().equals(other.tuitionType.getValue()))
			return false;
		return true;
	}	
	
	
}
