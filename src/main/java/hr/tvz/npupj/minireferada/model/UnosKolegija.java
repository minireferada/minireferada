package hr.tvz.npupj.minireferada.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "KOLEGIJ")
public class UnosKolegija {

	@Id
	@Column(name = "ID_KOLEGIJ")
	@GeneratedValue
	private int id;
	@Column(name = "NAZIV_KOLEGIJA")
	private String nazivKolegija;
	@Column(name = "OBAVEZNOST")
	private String obveznost;

	public UnosKolegija(String nazivKolegija, String obveznost, Semestar semestarKolegija) {

		this.nazivKolegija = nazivKolegija;
		this.obveznost = obveznost;
		this.semestarKolegija=semestarKolegija;
	}

	public String getNazivKolegija() {
		return nazivKolegija;
	}

	public void setNazivKolegija(String nazivKolegija) {
		this.nazivKolegija = nazivKolegija;
	}

	public String getObveznost() {
		return obveznost;
	}

	public void setObveznost(String obveznost) {
		this.obveznost = obveznost;
	}

	@ManyToOne
	@JoinColumn(name = "ID_SEMESTRA")
	private Semestar semestarKolegija;

	public UnosKolegija(){}
}
