package hr.tvz.npupj.minireferada.model;

import hr.tvz.npupj.minireferada.annotation.FxModelAttribute;

import java.io.Serializable;
import java.math.BigInteger;

import javafx.scene.control.Label;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MR_STUDENT_DATA")
public class Student implements Serializable{

	private static final long serialVersionUID = 1545761720198141190L;


	@Id
    @GeneratedValue
    private BigInteger id;


    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_FIRST_NAME")
    private String     firstname;

    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_LAST_NAME")
    private String     lastname;


	@FxModelAttribute(Label.class)
    @Column(name = "STUDENT_BIRTH_DATE")
    private String     birthDate;

    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_ADDRESS")
    private String     address;

    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_CITY")
    private String     city;

    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_POST_NUMBER")
    private String     postNumber;

    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_OIB")
    private String     oib;

    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_JMBAG")
    private String     jmbag;

    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_EMAIL")
    private String     email;

    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_TEL_NUMBER")
    private String     telNumber;
    
    @OneToOne(optional=false, fetch=FetchType.EAGER)
    @JoinColumn(name = "USER_ID", referencedColumnName="ID")
    private Osoba user;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostNumber() {
        return postNumber;
    }

    public void setPostNumber(String postNumber) {
        this.postNumber = postNumber;
    }

    public String getOib() {
        return oib;
    }

    public void setOib(String oib) {
        this.oib = oib;
    }

    public String getJmbag() {
        return jmbag;
    }

    public void setJmbag(String jmbag) {
        this.jmbag = jmbag;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }
    
    public Osoba getUser() {
		return user;
	}

	public void setUser(Osoba user) {
		this.user = user;
	}

	@Override
    public String toString() {
    	return firstname + " " + lastname;
    }
}
