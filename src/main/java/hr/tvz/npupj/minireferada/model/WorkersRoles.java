package hr.tvz.npupj.minireferada.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="WORKERS")
public class WorkersRoles {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;
	
	@Column(name="WORKPLACE_NAME")
	private String workplaceName;

	public String getWorkplaceName() {
		return workplaceName;
	}

	public void setWorkplaceName(String workplaceName) {
		this.workplaceName = workplaceName;
	}
	
	@OneToMany(mappedBy="workerRoles", fetch=FetchType.EAGER)
	private List<WorkersTvz> workers;
	
	public List<WorkersTvz> getWorkers() {
		return workers;
	}

	public void setWorkers(List<WorkersTvz> workers) {
		this.workers = workers;
	}

	@Override
	public String toString(){
		return this.workplaceName;
	}
	
}
