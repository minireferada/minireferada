package hr.tvz.npupj.minireferada.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Semestar {
	@Id
	@Column(name = "ID")
	@GeneratedValue
	private int id;

	@Column(name = "SEMESTAR_NAME")
	private String nazivSemestra;

	@OneToMany(mappedBy = "semestar", fetch = FetchType.LAZY)
	private List<Kolegij> kolegiji;

	public Semestar() {

	}

	public Semestar(String nazivSemestra) {
		super();
		this.nazivSemestra = nazivSemestra;
	}

	public List<Kolegij> getKolegiji() {
		return kolegiji;
	}

	public String getNazivSemestra() {
		return nazivSemestra;
	}

	public void setNazivSemestra(String nazivSemestra) {
		this.nazivSemestra = nazivSemestra;
	}
	
	@Override
	public String toString(){
		return this.nazivSemestra;
	}
}
