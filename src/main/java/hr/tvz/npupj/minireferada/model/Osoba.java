package hr.tvz.npupj.minireferada.model;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MR_USER")
public class Osoba {
    
	@Id
	@GeneratedValue
	private BigInteger id;
    
	@Column(name = "USER_NAME")
	private String  name;
    	
	@Column(name = "USER_PASSWORD")
	private String password;
    
	@Column(name = "USER_ROLE")
	private String role;

	@Column(name = "USER_ENABLED")
	private int enabled;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getRole(){
		return role;
	}
	
	public int getEnabled() {
		return enabled;
	}

	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}
}
