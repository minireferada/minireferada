package hr.tvz.npupj.minireferada.model;

import javax.persistence.*;

import java.math.BigInteger;

@Entity
@Table(name = "MR_SIGNED_EXAMS")
public class SignesdExams {

    @Id
    @GeneratedValue
    private BigInteger id;

    @Column(name = "SIGNED_EXAMS_ID")
    private String     signedExamID;
    
    @Column(name = "SIGNED_EXAMS_COURSE_NAME")
    private String     signedExamCourseName;
    
    @Column(name = "SIGNED_EXAMS_PROFESOR_NAME")
    private String     signedExamProfesor;
    
	@Column(name = "SIGNED_EXAMS_DUE_DATE")
	private String signedExamDueDate;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getSignedExamID() {
		return signedExamID;
	}

	public void setSignedExamID(String signedExamID) {
		this.signedExamID = signedExamID;
	}

	public String getSignedExamCourseName() {
		return signedExamCourseName;
	}

	public void setSignedExamCourseName(String signedExamCourseName) {
		this.signedExamCourseName = signedExamCourseName;
	}

	public String getSignedExamProfesor() {
		return signedExamProfesor;
	}

	public void setSignedExamProfesor(String signedExamProfesor) {
		this.signedExamProfesor = signedExamProfesor;
	}

	public String getSignedExamDueDate() {
		return signedExamDueDate;
	}

	public void setSignedExamDueDate(String signedExamDueDate) {
		this.signedExamDueDate = signedExamDueDate;
	}
	

}
