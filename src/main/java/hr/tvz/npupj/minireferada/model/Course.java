package hr.tvz.npupj.minireferada.model;

import javax.persistence.*;
import java.math.BigInteger;

/**
 * Created by mjovanovic on 29/05/15.
 */
@Entity
@Table(name = "MR_COURSE")
public class Course {

    @Id
    @GeneratedValue
    private BigInteger id;

    @Column(name = "COURSE_NAME")
    private String     name;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
