package hr.tvz.npupj.minireferada.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by mjovanovic on 23/05/15.
 */

@Entity
public class Kolegij {

	@Id
	@Column(name = "ID_KOLEGIJ")
	@GeneratedValue
	private int id;

	@Column(name = "NAME_COURSE")
	private String naziv;

	@Column(name = "GRADE")
	private int ocjena;

	@Column(name = "RIGHTS_FOR_SIGN")
	private String pravoNaPotpis;

	public Kolegij() { }

	public Kolegij(String naziv, String pravoNaPotpis, int ocjena) {
		this.naziv = naziv;
		this.ocjena = ocjena;
		this.pravoNaPotpis = pravoNaPotpis;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getOcjena() {
		return ocjena;
	}

	public void setOcjena(int ocjena) {
		this.ocjena = ocjena;
	}

	public String getPravoNaPotpis() {
		return pravoNaPotpis;
	}

	public void setPravoNaPotpis(String pravoNaPotpis) {
		this.pravoNaPotpis = pravoNaPotpis;
	}

	@ManyToOne
	@JoinColumn(name = "ID_SEMESTAR")
	private Semestar semestar;
}
