package hr.tvz.npupj.minireferada.model;

import hr.tvz.npupj.minireferada.annotation.FxModelAttribute;
import javafx.scene.control.Label;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "MR_STUDENT_ADMISSION_DATA")
public class StudentAdmission {

    @Id
    @GeneratedValue
    private BigInteger id;

    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_ADMISSION_DATE")
    private String     admissionDate;
    
    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_ACADEMIC_YEAR")
    private String     academicYear;
    
    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_TEACHING_YEAR")
    private String     teachingYear;

    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_SEMESTER")
    private String     semester;
    
    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_STUDY")
    private String     study;
    
    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_STUDY_COURSE")
    private String     studyCurse;
    
    
    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_METHOD_OF_ADMISSION")
    private String     admissionMethod;
    
    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_TUITION_FEE")
    private String     tuitionFee;
    
    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_ETSC_PRICE")
    private String     etscPrice;
    
    @FxModelAttribute(Label.class)
    @Column(name = "STUDENT_STUDENT_RIGHTS")
    private String     rights;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getAdmissionDate() {
		return admissionDate;
	}

	public void setAdmissionDate(String admissionDate) {
		this.admissionDate = admissionDate;
	}

	public String getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	public String getTeachingYear() {
		return teachingYear;
	}

	public void setTeachingYear(String teachingYear) {
		this.teachingYear = teachingYear;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public String getStudy() {
		return study;
	}

	public void setStudy(String study) {
		this.study = study;
	}

	public String getStudyCurse() {
		return studyCurse;
	}

	public void setStudyCurse(String studyCurse) {
		this.studyCurse = studyCurse;
	}

	public String getAdmissionMethod() {
		return admissionMethod;
	}

	public void setAdmissionMethod(String admissionMethod) {
		this.admissionMethod = admissionMethod;
	}

	public String getTuitionFee() {
		return tuitionFee;
	}

	public void setTuitionFee(String tuitionFee) {
		this.tuitionFee = tuitionFee;
	}

	public String getEtscPrice() {
		return etscPrice;
	}

	public void setEtscPrice(String etscPrice) {
		this.etscPrice = etscPrice;
	}

	public String getRights() {
		return rights;
	}

	public void setRights(String rights) {
		this.rights = rights;
	}
    
}
