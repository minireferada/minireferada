package hr.tvz.npupj.minireferada.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="WORKERS_ON_TVZ")
public class WorkersTvz {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;
	
	@Column(name="WORKER_NAME")
	private String workerName;
	
	@Column(name="WORKER_LAST_NAME")
	private String workerLastName;

	@Column(name="DATE_OF_BIRTH")
	private Date dateOfBirth;
	
	@Column(name="OIB")
	private String OIB;
	
	@ManyToOne
	@JoinColumn(name="ID_WORKER")
	private WorkersRoles workerRoles;
	
	public WorkersTvz(){}

	public WorkersTvz(String workerName, String workerLastName,
			Date dateOfBirth, String oIB, WorkersRoles workerRole) {

		this.workerName = workerName;
		this.workerLastName = workerLastName;
		this.dateOfBirth = dateOfBirth;
		OIB = oIB;
		this.workerRoles = workerRole;
	
	}

	public String getWorkerName() {
		return workerName;
	}

	public void setWorkerName(String workerName) {
		this.workerName = workerName;
	}

	public String getWorkerLastName() {
		return workerLastName;
	}

	public void setWorkerLastName(String workerLastName) {
		this.workerLastName = workerLastName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getOIB() {
		return OIB;
	}

	public void setOIB(String oIB) {
		OIB = oIB;
	}
}
