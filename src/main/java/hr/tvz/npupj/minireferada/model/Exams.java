package hr.tvz.npupj.minireferada.model;

import javax.persistence.*;

import java.math.BigInteger;

@Entity
@Table(name = "MR_EXAMS")
public class Exams {

    @Id
    @GeneratedValue
    private BigInteger id;

    @Column(name = "EXAM_ID")
    private String     examID;
    
    @Column(name = "EXAM_COURSE_NAME")
    private String     examCourseName;
    
    @Column(name = "EXAM_PROFESOR_NAME")
    private String     examProfesor;
    
	@Column(name = "EXAM_DUE_DATE")
	private String examDueDate;
	
	@Column(name = "EXAM_REGISTRATION_DATE")
	private String examRegistrationDate;
	
	@Column(name = "EXAM_CANCELLATION_DATE")
	private String examCancellationDate;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getExamID() {
		return examID;
	}

	public void setExamID(String examID) {
		this.examID = examID;
	}

	public String getExamCourseName() {
		return examCourseName;
	}

	public void setExamCourseName(String examCourseName) {
		this.examCourseName = examCourseName;
	}

	public String getExamProfesor() {
		return examProfesor;
	}

	public void setExamProfesor(String examProfesor) {
		this.examProfesor = examProfesor;
	}

	public String getExamDueDate() {
		return examDueDate;
	}

	public void setExamDueDate(String examDueDate) {
		this.examDueDate = examDueDate;
	}

	public String getExamRegistrationDate() {
		return examRegistrationDate;
	}

	public void setExamRegistrationDate(String examRegistrationDate) {
		this.examRegistrationDate = examRegistrationDate;
	}

	public String getExamCancellationDate() {
		return examCancellationDate;
	}

	public void setExamCancellationDate(String examCancellationDate) {
		this.examCancellationDate = examCancellationDate;
	}


}
