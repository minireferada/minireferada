package hr.tvz.npupj.minireferada.config;

import hr.tvz.npupj.minireferada.MiniReferada;
import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;

import java.io.IOException;
import java.net.URL;

/**
 * Created by mjovanovic on 19/05/15.
 */
public class FxmlScreen extends Stage {

    private static final Logger      LOGGER = LoggerFactory.getLogger(FxmlScreen.class);

    private AbstractScreenController controller;

    public FxmlScreen(AbstractScreenController controller) {
        this(controller, StageStyle.DECORATED);
    }

    public FxmlScreen(final AbstractScreenController controller, StageStyle style) {
        super(style);
        this.controller = controller;

        LOGGER.debug("Initialize screen for controller [{}]", controller.getClass().getSimpleName());

        FXMLLoader loader = new FXMLLoader(loadFxmlByControllerName());
        loader.setControllerFactory((clazz) -> {
            return controller;
        });

        try {
            Parent stage = (Parent) loader.load();
            setScene(new Scene(stage));

            this.controller.setScreen(this);
            this.controller.onCreate();
        } catch (IOException e) {
            LOGGER.error("Fail to initialize screen", e);
        }
    }

    public void hideAndShow(FxmlScreen previousScreen) {
        hideAndShow(previousScreen, null);
    }

    public void openModal(FxmlScreen parentScreen) {
        openModal(parentScreen, null);
    }

    public void hideAndShow(FxmlScreen previousScreen, ModelMap modelMap) {
        this.controller.onShow(modelMap);
        this.show();
        previousScreen.getController().onHide(modelMap);
        previousScreen.hide();
    }

    public void openModal(FxmlScreen parentScreen, ModelMap modelMap) {
        Stage modalStage = new Stage();
        modalStage.setScene(this.getScene());
        modalStage.initOwner(parentScreen);
        modalStage.initModality(Modality.WINDOW_MODAL);

        this.controller.onShow(modelMap);
        modalStage.show();
    }

    private URL loadFxmlByControllerName() {
        String clazzName = this.controller.getClass().getSimpleName();
        String fxmlName = StringUtils.substringBefore(clazzName, "Controller");
        fxmlName = StringUtils.uncapitalize(fxmlName) + "Screen.fxml";

        LOGGER.debug("Loading screen [{}]", fxmlName);

        return MiniReferada.class.getResource("/fxml/" + fxmlName);
    }

    protected AbstractScreenController getController() {
        return controller;
    }
}
