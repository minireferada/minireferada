package hr.tvz.npupj.minireferada.config;

import org.springframework.context.ApplicationContext;

/**
 * Created by mjovanovic on 30/05/15.
 */
public class SpringApplicationContextHolder {

    private static ApplicationContext appCtx;

    public SpringApplicationContextHolder(ApplicationContext appCtx) {
        SpringApplicationContextHolder.appCtx = appCtx;
    }

    public static ApplicationContext getAppCtx() {
        return appCtx;
    }

}
