package hr.tvz.npupj.minireferada.config;

import hr.tvz.npupj.minireferada.model.Kolegij;
import hr.tvz.npupj.minireferada.model.Osoba;
import hr.tvz.npupj.minireferada.model.Semestar;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.SessionFactoryObserver;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateConfiguration {

	private static final SessionFactory sessionFactory;
	private static final ServiceRegistry serviceRegistry;

	static {
		try {
			Configuration config = getConfiguration();
			serviceRegistry = new StandardServiceRegistryBuilder()
					.applySettings(config.getProperties()).build();
			config.setSessionFactoryObserver(new SessionFactoryObserver() {
				private static final long serialVersionUID = 1L;

				@Override
				public void sessionFactoryCreated(SessionFactory factory) {
				}

				@Override
				public void sessionFactoryClosed(SessionFactory factory) {
					StandardServiceRegistryBuilder.destroy(serviceRegistry);

				}
			});
			sessionFactory = config.buildSessionFactory(serviceRegistry);
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed" + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static Session openSession() {
		return sessionFactory.openSession();
	}

	private static Configuration getConfiguration() {
		Configuration cfg = new Configuration();
		cfg.addAnnotatedClass(Kolegij.class);
		cfg.addAnnotatedClass(Semestar.class);
		cfg.addAnnotatedClass(Osoba.class);
		cfg.setProperty("hibernate.connection.driver_class", "org.h2.Driver");
		cfg.setProperty("hibernate.connection.url",
				"jdbc:h2:tcp://localhost/~/Java-2015");
		cfg.setProperty("hibernate.connection.username", "kristijan");
		cfg.setProperty("hibernate.connection.password", "kristijan");
		cfg.setProperty("hibernate.show_sql", "true");
		cfg.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
		cfg.setProperty("hibernate.cache.provider_class",
				"org.hibernate.cache.NoCacheProvider");
		cfg.setProperty("hibernate.hbm2ddl.auto",
				"update");
		
		

		return cfg;
	}

}
