package hr.tvz.npupj.minireferada.config;

/**
 * Created by mjovanovic on 13/06/15.
 */
public final class UserRole {
    public static final String ROLE_STUDENT  = "ROLE_STUDENT";
    public static final String ROLE_REFERADA = "ROLE_REFERADA";
    public static final String ROLE_PROFESOR = "ROLE_PROFESOR";
    public static final String ROLE_ADMIN    = "ROLE_ADMIN";
}
