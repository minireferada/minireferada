package hr.tvz.npupj.minireferada.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Created by mjovanovic on 19/05/15.
 */
@EnableJpaRepositories("hr.tvz.npupj.minireferada.dao")
@ComponentScan("hr.tvz.npupj.minireferada")
@Configuration
@ImportResource("classpath:/config/appCtx-security.xml")
public class SpringConfiguration {

    public static final String PROFILE_LOCAL_NAME  = "local";
    public static final String PROFILE_REMOTE_NAME = "remote";

    @Bean
    public SpringApplicationContextHolder applicationContextHolder(ApplicationContext appCtx) {
        return new SpringApplicationContextHolder(appCtx);
    }

    // Hibernate config

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    // Hibernate config

    @Bean
    public PlatformTransactionManager transactionManager(DataSource dataSource) {
        EntityManagerFactory factory = entityManagerFactory(dataSource).getObject();
        return new JpaTransactionManager(factory);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setGenerateDdl(false);
        adapter.setShowSql(true);

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setDataSource(dataSource);
        factory.setJpaVendorAdapter(adapter);
        factory.setPackagesToScan("hr.tvz.npupj.minireferada.model");
        factory.afterPropertiesSet();

        return factory;
    }

    @Configuration
    @Profile(PROFILE_REMOTE_NAME)
    public static class RemoteConfiguration {

        @Bean
        public DataSource dataSource() {
            DriverManagerDataSource dataSource = new DriverManagerDataSource();
            dataSource.setDriverClassName("com.mysql.jdbc.Driver");
            dataSource.setUrl("jdbc:mysql://pma.test.ofca.ninja:3306/MRTIM2?useUnicode=true&characterEncoding=UTF-8&characterSetResults=UTF-8");
            dataSource.setUsername("MRTIM2");
            dataSource.setPassword("MRTIM2");

            return dataSource;
        }

    }

    @Configuration
    @Profile(PROFILE_LOCAL_NAME)
    public static class LocalConfiguration {

        @Bean
        public DataSource dataSource() {
            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
            builder.setType(EmbeddedDatabaseType.H2);
            builder.addScript("/sql/database.ddl");
            builder.addScript("/sql/import.sql");
            return builder.build();

        }

    }
}
