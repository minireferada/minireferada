package hr.tvz.npupj.minireferada.controller.referada;

import hr.tvz.npupj.minireferada.common.DataCollector;
import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import hr.tvz.npupj.minireferada.controller.common.ValidationResult;
import hr.tvz.npupj.minireferada.model.Student;
import hr.tvz.npupj.minireferada.model.TuitionFee;
import hr.tvz.npupj.minireferada.service.StudentService;
import hr.tvz.npupj.minireferada.service.TuitionFeeService;
import hr.tvz.npupj.minireferada.util.DateUtils;

import java.math.BigDecimal;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

@Controller
public class NewInvoiceController extends AbstractScreenController {

	@FXML
	private TextArea feeDesc;
	@FXML
	private ComboBox<String> feeType;
	@FXML
	private ComboBox<String> feeStatus;
	@FXML
	private ComboBox<Student> feeStudent;
	@FXML
	private TextField feeAmount;
	@FXML
	private DatePicker feeDueDate;

	@Autowired
	private StudentService studentService;
	@Autowired
	private TuitionFeeService tuitionFeeService;

	@Override
	public void onShow(ModelMap modelMap) {
		super.onShow(modelMap);
		feeStatus.setItems(DataCollector.getFeeStatusList());
		feeStatus.getSelectionModel().select("DODJELA");
		feeType.setItems(DataCollector.getFeeTypeList());
		feeStudent.setItems(FXCollections.observableArrayList(studentService
				.getAllStudents()));
	}

	@FXML
	public void addInvoice() {
		ValidationResult vr = validateForm();
		if (vr.isValid()) {
			TuitionFee newInvoice = fillData();
			tuitionFeeService.addNewInvoice(newInvoice);
		} else {
			showMessageScreen(vr.getMessage());
		}
	}

	private ValidationResult validateForm() {
		ValidationResult vr = new ValidationResult(true, null);
		if (feeDesc.getText() == null || feeDesc.getText().isEmpty()) {
			vr.setValid(false);
			vr.addMessage("Upišite opis uplatnice");
		}
		if (feeStudent.getSelectionModel().getSelectedItem() == null) {
			vr.setValid(false);
			vr.addMessage("Odaberite studenta");
		}
		if (feeStatus.getSelectionModel().getSelectedItem() == null) {
			vr.setValid(false);
			vr.addMessage("Odaberite status uplatnice");
		}
		if (feeAmount.getText() == null || feeAmount.getText().isEmpty()) {
			vr.setValid(false);
			vr.addMessage("Unesite iznos uplatnice");
		} else {
			try {
				Double.parseDouble(feeAmount.getText());
			} catch (NumberFormatException e) {
				vr.setValid(false);
				vr.addMessage("Krivi format iznosa, iznos mora biti tipa 200");
			}
		}
		if (feeDueDate.getValue() == null) {
			vr.setValid(false);
			vr.addMessage("Odaberite datum dospijeća");
		}
		if (feeType.getSelectionModel().getSelectedItem() == null) {
			vr.setValid(false);
			vr.addMessage("Odaberite tip uplatnice");
		}
		return vr;
	}

	private TuitionFee fillData() {
		TuitionFee tf = new TuitionFee();
		tf.setDescription(feeDesc.getText());
		tf.setFeeAmount(BigDecimal.valueOf(Double.parseDouble(feeAmount
				.getText())));
		tf.setStatus(feeStatus.getSelectionModel().getSelectedItem());
		tf.setStudent(feeStudent.getSelectionModel().getSelectedItem());
		tf.setTuitionType(feeType.getSelectionModel().getSelectedItem());
		tf.setDueDate(DateUtils.getDateFromDatePicker(feeDueDate));
		tf.setPaidAmount(BigDecimal.ZERO);
		return tf;
	}
}
