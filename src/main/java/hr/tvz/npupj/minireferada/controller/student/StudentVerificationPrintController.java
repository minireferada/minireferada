package hr.tvz.npupj.minireferada.controller.student;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import hr.tvz.npupj.minireferada.model.Student;
import hr.tvz.npupj.minireferada.model.StudentAdmission;
import hr.tvz.npupj.minireferada.util.FxModelMapper;

@Controller
public class StudentVerificationPrintController extends AbstractScreenController {

    @FXML
    private Label               lblFirstname;
    @FXML
    private Label               lblLastname;
    @FXML
    private Label               lblFirstname1;
    @FXML
    private Label               lblLastname1;
    
    @FXML
    private Label               lblBirthDate;

    @FXML
    private Label               lblCity;


    @FXML
    private Label               lblJmbag;

    

    @FXML
    private Label               lblAcademicYear;
    @FXML
    private Label               lblTeachingYear;

    @FXML
    private Label               lblStudy;

	
    @Override
    public void onShow(ModelMap modelMap) {
    	Student podaciStudent = (Student) modelMap.get("student");

       	FxModelMapper.map(this, podaciStudent);
       	lblFirstname1.setText(podaciStudent.getFirstname());
       	lblLastname1.setText(podaciStudent.getLastname());
    	
    	StudentAdmission podaciUpis = (StudentAdmission) modelMap.get("studentAdmission");
    	//lblTeachingYear.setText(podaciUpis.getTeachingYear());

       	FxModelMapper.map(this, podaciUpis);
    }

}
