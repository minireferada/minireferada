package hr.tvz.npupj.minireferada.controller.referada;

import hr.tvz.npupj.minireferada.common.Constants;
import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import hr.tvz.npupj.minireferada.model.TuitionFee;
import hr.tvz.npupj.minireferada.model.TuitionFeeView;
import hr.tvz.npupj.minireferada.service.TuitionFeeService;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

@Component
public class InvoiceImageController extends AbstractScreenController {

	@FXML
	ImageView imgInvoice;
	
	@Autowired
	TuitionFeeService tuitionFeeService;
	
	private TuitionFee fee;
	private ObservableList<TuitionFeeView> loadedInvoiceLs;
	@Override
	public void onShow(ModelMap modelMap) {
		super.onShow(modelMap);
		if (modelMap == null) {
			return;
		}
		Integer feeID = (Integer) modelMap.get(Constants.BUTTON_MODEL_VALUE);
		loadedInvoiceLs = (ObservableList<TuitionFeeView>) modelMap.get(Constants.LOADED_INVOICE_LIST_MODEL_DATA_TAG);
		fee = tuitionFeeService.getTuitionFeeById(feeID);
		try {
			Image img = new Image(new FileInputStream(fee.getFilePath()));
			imgInvoice.setImage(img);
		} catch (FileNotFoundException e) {
			System.out.println("No image " + e.getMessage());
		} 
		
	}
	
	@FXML
	public void confirmInvoice() {
		TuitionFeeView tfv = new TuitionFeeView(fee);
		loadedInvoiceLs.remove(tfv); 
		tuitionFeeService.confirmInvoicePayment(fee);
		getScreen().close();
	}
	
	@FXML
	public void sendInvoiceBack() {
		TuitionFeeView tfv = new TuitionFeeView(fee);
		loadedInvoiceLs.remove(tfv);
		tuitionFeeService.sendInvoiceBackToStudent(fee);
		getScreen().close();
	}
}
