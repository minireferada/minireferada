package hr.tvz.npupj.minireferada.controller.student;

import java.math.BigInteger;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

import hr.tvz.npupj.minireferada.common.Constants;
import hr.tvz.npupj.minireferada.component.TableColumnButtonCell;
import hr.tvz.npupj.minireferada.component.TableColumnButtonCell.ButtonConfig;
import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import hr.tvz.npupj.minireferada.model.Course;
import hr.tvz.npupj.minireferada.service.CourseService;

/**
 * Created by mjovanovic on 26/05/15.
 */
@Controller
public class StudentSignedCoursesController extends AbstractScreenController {

    private static final Logger             LOGGER = LoggerFactory.getLogger(StudentSignedCoursesController.class);

    @Autowired
    private CourseService                   courseService;

    @FXML
    private TableView<Course>               tblSignedCourses;

    @FXML
    private TableColumn<Course, String>     tclSignedCoursesCourse;

    @FXML
    private TableColumn<Course, BigInteger> tclSignedCoursesDetails;

    @Override
    public void onShow(ModelMap p_modelMap) {

        List<Course> allCourses = courseService.getAllCourses();
        ObservableList<Course> courses = FXCollections.observableArrayList(allCourses);

        tclSignedCoursesCourse.setCellValueFactory(new PropertyValueFactory<Course, String>("name"));
        tclSignedCoursesDetails.setCellValueFactory(new PropertyValueFactory<Course, BigInteger>("id"));

        ButtonConfig config = new ButtonConfig("Detalji", (event, modelMap) -> {
            LOGGER.debug("{}", modelMap.get(Constants.BUTTON_MODEL_VALUE));
        });

        tclSignedCoursesDetails.setCellFactory(new TableColumnButtonCell<Course, BigInteger>(config));

        tblSignedCourses.setItems(courses);
    }
}
