package hr.tvz.npupj.minireferada.controller.student;

import java.math.BigInteger;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

import hr.tvz.npupj.minireferada.component.TableColumnButtonCell;
import hr.tvz.npupj.minireferada.component.TableColumnButtonCell.ButtonConfig;
import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import hr.tvz.npupj.minireferada.model.SignedExams;
import hr.tvz.npupj.minireferada.service.SignedExamsService;

@Controller
public class StudentSignedExamsScreenController extends AbstractScreenController {
	
	 private static final Logger LOGGER = LoggerFactory.getLogger(StudentCoursesExamsController.class);
	    
	    @Autowired
	    private SignedExamsService                   signedExamsService;

	    @FXML
	    private TableView<SignedExams>               tblSignedExams;

	    @FXML
	    private TableColumn<SignedExams, String>     tclSignedExamID;
	    
	    @FXML
	    private TableColumn<SignedExams, String>     tclSignedExamCourseName;
	    
	    @FXML
	    private TableColumn<SignedExams, String>     tclSignedExamProfesorName;
	    
	    @FXML
	    private TableColumn<SignedExams, String>     tclSignedExamDueDate;
	    
	    
	    @FXML
	    private TableColumn<SignedExams, String> tclSignedExamOdjava;
	    
	    @Override
	    public void onShow(ModelMap p_modelMap) {

	        List<SignedExams> allSignedExams = signedExamsService.getAllSignedExams();
	        ObservableList<SignedExams> signedExams = FXCollections.observableArrayList(allSignedExams);

	        tclSignedExamID.setCellValueFactory(new PropertyValueFactory<SignedExams, String>("signedExamID"));
	        tclSignedExamCourseName.setCellValueFactory(new PropertyValueFactory<SignedExams, String>("signedExamCourseName"));
	        tclSignedExamProfesorName.setCellValueFactory(new PropertyValueFactory<SignedExams, String>("signedExamProfesor"));
	        tclSignedExamDueDate.setCellValueFactory(new PropertyValueFactory<SignedExams, String>("signedExamDueDate"));


	        tclSignedExamOdjava.setCellValueFactory(new PropertyValueFactory<SignedExams, String>("signedExamID"));
	        ButtonConfig config = new ButtonConfig("Odjavi", (event, modelMap) -> {
	            LOGGER.debug("{}", modelMap.get("cellValue"));
	        });

	        tclSignedExamOdjava.setCellFactory(new TableColumnButtonCell<SignedExams, String>(config));

	        tblSignedExams.setItems(signedExams);
	    }

}
