package hr.tvz.npupj.minireferada.controller.student;


import hr.tvz.npupj.minireferada.common.Constants;
import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import hr.tvz.npupj.minireferada.model.TuitionFee;
import hr.tvz.npupj.minireferada.service.TuitionFeeService;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

@Controller
public class GenerateInvoiceController extends AbstractScreenController {
	
	@Autowired
	TuitionFeeService tuiService;
	
	@FXML
	Label lblPayerName;
	@FXML
	Label lblPayerAddress;
	@FXML
	Label lblReciverName;
	@FXML
	Label lblReciverAddress;
	@FXML
	Label lblInvoiceCallingNumber;
	@FXML
	Label lblReciverAccountNumber;
	@FXML
	Label lblDescription;
 	
	private TuitionFee stuTuiFee;
	
	@Override
	public void onShow(ModelMap modelMap) {
		Object cellValue = modelMap.get(Constants.BUTTON_MODEL_VALUE);
		if (cellValue != null) {
			if (cellValue instanceof Integer) {
				stuTuiFee = tuiService.getTuitionFeeById((Integer)cellValue);
			} else {
				throw new IllegalStateException(String.format("%s ", GenerateInvoiceController.class.getName()));
			}
		}
		
		lblPayerName.setText(stuTuiFee.getStudent().getFirstname());
		lblPayerAddress.setText(stuTuiFee.getStudent().getLastname());
		lblReciverName.setText("Tehničko veleučilište u Zagrebu");
		lblInvoiceCallingNumber.setText("02032939393");
		lblReciverAccountNumber.setText("322324242424242");
		lblDescription.setText(stuTuiFee.getDescription());
		super.onShow(modelMap);
	}
	
}
