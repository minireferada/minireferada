package hr.tvz.npupj.minireferada.controller.student;

import hr.tvz.npupj.minireferada.baza.DateUtils;
import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import hr.tvz.npupj.minireferada.model.Kolegij;
import hr.tvz.npupj.minireferada.model.Semestar;
import hr.tvz.npupj.minireferada.service.KolegijService;
import hr.tvz.npupj.minireferada.service.SemestarService;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by mjovanovic on 26/05/15.
 */
@Component
public class StudentGradesController extends AbstractScreenController {
	
	@Autowired
	private KolegijService kolegijService;
	
	@Autowired
	private SemestarService semestarService;

	@FXML
	private TableView<Kolegij> tblViewCourserPreview;

	@FXML
	private TableColumn<Kolegij, String> tblColumnCourses;

	@FXML
	private TableColumn<Kolegij, String> tblColumnRightsForSign;

	@FXML
	private TableColumn<Kolegij, String> tblColumnGrade;

	@FXML
	private ComboBox<Semestar> cmbOdabirSemestra;

	ArrayList<Semestar> semestars;

	private void populateTable(List<Kolegij> kolegijiSemestra) {
		ObservableList<Kolegij> data = FXCollections
				.observableList(kolegijiSemestra);

		tblColumnCourses
				.setCellValueFactory(new PropertyValueFactory<Kolegij, String>(
						"naziv"));
		tblColumnRightsForSign
				.setCellValueFactory(new PropertyValueFactory<Kolegij, String>(
						"pravoNaPotpis"));
		tblColumnGrade
				.setCellValueFactory(new PropertyValueFactory<Kolegij, String>(
						"ocjena"));

		tblViewCourserPreview.setItems(data);

		tblColumnCourses.setStyle("-fx-alignment: LEFT;");
		tblColumnRightsForSign.setStyle("-fx-alignment: CENTER;");
		tblColumnGrade.setStyle("-fx-alignment: CENTER;");
	}

	@FXML
	public void switchCourses() {
		
		Semestar odabraniSemestar = cmbOdabirSemestra
				.getSelectionModel().getSelectedItem();
		
//		populateTable(odabraniSemestar.getKolegiji()); -> USE WITH EAGER LOAD
		populateTable(kolegijService.getKolegijBySemestar(odabraniSemestar));	// LAZY LOAD
	}

	@FXML
	public void fillCombos() {
		List<Semestar> semestars = semestarService.getAllSemestars();
		cmbOdabirSemestra
				.setItems(FXCollections.observableArrayList(semestars));
	}

}
