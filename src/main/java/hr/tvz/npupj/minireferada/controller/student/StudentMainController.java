package hr.tvz.npupj.minireferada.controller.student;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import org.springframework.stereotype.Component;

import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;

@Component
public class StudentMainController extends AbstractScreenController {

    @FXML
    public void showUserData(ActionEvent actionEvent) {
        showModal("studentDataScreen");
    }

    @FXML
    public void showSignedCourses(ActionEvent actionEvent) {
        showModal("studentSignedCoursesScreen");
    }

    @FXML
    public void showCoursesExams(ActionEvent actionEvent) {
        showModal("studentCoursesExamsScreen");
    }

    @FXML
    public void showGrades(ActionEvent actionEvent) {
        showModal("studentGradesScreen");
    }

    @FXML
    public void showTuitionFees(ActionEvent actionEvent) {
        showModal("studentTuitionFeesScreen");
    }
    
    @FXML
    public void performLogout(ActionEvent actionEvent) {
    	switchTo("loginScreen");
    }
}
