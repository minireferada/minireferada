package hr.tvz.npupj.minireferada.controller.student;

import java.math.BigInteger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import hr.tvz.npupj.minireferada.model.Student;
import hr.tvz.npupj.minireferada.model.StudentAdmission;
import hr.tvz.npupj.minireferada.service.StudentAdmissionService;
import hr.tvz.npupj.minireferada.service.StudentService;
import hr.tvz.npupj.minireferada.util.FxModelMapper;

/**
 * Created by mjovanovic on 26/05/15.
 */
@Component
public class StudentDataController extends AbstractScreenController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentDataController.class);

    @Autowired
    private StudentService      studentService;
    
    @Autowired
    private StudentAdmissionService      studentAdmissionService;

    @FXML
    private Label               lblFirstname;
    @FXML
    private Label               lblLastname;
    @FXML
    private Label               lblBirthDate;
    @FXML
    private Label               lblAddress;
    @FXML
    private Label               lblCity;
    @FXML
    private Label               lblPostNumber;
    @FXML
    private Label               lblOib;
    @FXML
    private Label               lblJmbag;
    @FXML
    private Label               lblEmail;
    @FXML
    private Label               lblTelNumber;
    
    @FXML
    private Label               lblAdmissionDate; 
    @FXML
    private Label               lblAcademicYear;
    @FXML
    private Label               lblTeachingYear;
    @FXML
    private Label               lblSemester;
    @FXML
    private Label               lblStudy;
    @FXML
    private Label               lblStudyCurse;
    @FXML
    private Label               lblAdmissionMethod;
    @FXML
    private Label               lblTuitionFee;
    @FXML
    private Label               lblEtscPrice;
    @FXML
    private Label               lblRights;
    
    //Student - ekran za promjenu lozinke
    
    @FXML
    private PasswordField       txtPassword1;
    @FXML
    private PasswordField       txtPassword2;
    @FXML
    private PasswordField       txtPasswordOld;
    @FXML
    public Button btnSave;
    @FXML
    public Button btnCancel;
   
    /*
    @FXML
    public void handleCancelButtonAction(ActionEvent event) {
    	Stage stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }
    */
    
    
   // Student - ekran za ispis potvrde
    
    @FXML
	public void showLoginDataScreen(ActionEvent event) {
		showModal("studentLoginDataScreen");
	}
    
    
    
    /*
    @FXML
   	public void showVerificationPrintScreen(ActionEvent event) {
   		showModal("studentVerificationPrintScreen");
   	}
   	*/
    
    @FXML
   	public void showVerificationPrintScreen(ActionEvent event) {
    	ModelMap modelMap = new ModelMap();
    	StudentAdmission studentAdmission = studentAdmissionService.getStudentAdmissionById( BigInteger.valueOf(1) ); 
    	modelMap.addAttribute("studentAdmission", studentAdmission);
    	
    	Student student = studentService.getStudentByJmbag("98546783223");
    	modelMap.addAttribute("student", student);
    	
    	showModal("studentVerificationPrintScreen", modelMap);
   	}

    
    
    
    
    
    @Override
    public void onShow(ModelMap p_modelMap) {

        Student student = studentService.getStudentByJmbag("98546783223");

        FxModelMapper.map(this, student);
        
        
		StudentAdmission studentAdmission = studentAdmissionService.getStudentAdmissionById( BigInteger.valueOf(1) ); 

        FxModelMapper.map(this, studentAdmission);
       
    }
    

 

}
