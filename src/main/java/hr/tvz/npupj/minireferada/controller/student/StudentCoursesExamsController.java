package hr.tvz.npupj.minireferada.controller.student;

import java.math.BigInteger;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import hr.tvz.npupj.minireferada.component.TableColumnButtonCell;
import hr.tvz.npupj.minireferada.component.TableColumnButtonCell.ButtonConfig;
import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import hr.tvz.npupj.minireferada.model.Exams;
import hr.tvz.npupj.minireferada.service.ExamsService;

/**
 * Created by mjovanovic on 26/05/15.
 */
@Component
public class StudentCoursesExamsController extends AbstractScreenController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentCoursesExamsController.class);
    
    @Autowired
    private ExamsService                   examsService;

    @FXML
    private TableView<Exams>               tblExams;

    @FXML
    private TableColumn<Exams, String>     tclExamID;
    
    @FXML
    private TableColumn<Exams, String>     tclExamCourseName;
    
    @FXML
    private TableColumn<Exams, String>     tclExamProfesorName;
    
    @FXML
    private TableColumn<Exams, String>     tclExamDueDate;
    
    @FXML
    private TableColumn<Exams, String>     tclExamRegistrationDate;
    
    @FXML
    private TableColumn<Exams, String>     tclExamCancellationDate;
    
    @FXML
    private TableColumn<Exams, String> tclExamPrijava;


    
    @FXML
	public void showSignedExamsDataScreen(ActionEvent event) {
		showModal("studentSignedExamsScreenScreen");
	}
    
    @Override
    public void onShow(ModelMap p_modelMap) {

        List<Exams> allExams = examsService.getAllExams();
        ObservableList<Exams> exams = FXCollections.observableArrayList(allExams);

        tclExamID.setCellValueFactory(new PropertyValueFactory<Exams, String>("examID"));
        tclExamCourseName.setCellValueFactory(new PropertyValueFactory<Exams, String>("examCourseName"));
        tclExamProfesorName.setCellValueFactory(new PropertyValueFactory<Exams, String>("examProfesor"));
        tclExamDueDate.setCellValueFactory(new PropertyValueFactory<Exams, String>("examDueDate"));
        tclExamRegistrationDate.setCellValueFactory(new PropertyValueFactory<Exams, String>("examRegistrationDate"));
        tclExamCancellationDate.setCellValueFactory(new PropertyValueFactory<Exams, String>("examCancellationDate"));

        tclExamPrijava.setCellValueFactory(new PropertyValueFactory<Exams, String>("examID"));
        ButtonConfig config = new ButtonConfig("Prijavi", (event, modelMap) -> {
            LOGGER.debug("{}", modelMap.get("cellValue"));
        });

        tclExamPrijava.setCellFactory(new TableColumnButtonCell<Exams, String>(config));

        tblExams.setItems(exams);
    }
	
	
}
