package hr.tvz.npupj.minireferada.controller.admin;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import org.springframework.stereotype.Component;

import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;

@Component
public class AdminMainController extends AbstractScreenController {
	@FXML
    public void performLogout(ActionEvent actionEvent) {
    	switchTo("loginScreen");
    }
}
