package hr.tvz.npupj.minireferada.controller.student;

import hr.tvz.npupj.minireferada.common.Constants;
import hr.tvz.npupj.minireferada.common.DataCollector;
import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import hr.tvz.npupj.minireferada.model.TuitionFee;
import hr.tvz.npupj.minireferada.model.TuitionFeeView;
import hr.tvz.npupj.minireferada.service.TuitionFeeService;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.stage.FileChooser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

@Controller
public class LoadInvoiceController extends AbstractScreenController {

	@Autowired
	TuitionFeeService tuitionFeeService;

	@FXML
	private Label lblFileName;

	private BigInteger tuitionFeeID;
	private ObservableList<TuitionFeeView> tuitionFeeViewList;
	private TableView<TuitionFeeView> tuitionFeeTable;

	@Override
	public void onShow(ModelMap modelMap) {
		super.onShow(modelMap);
		this.tuitionFeeID = BigInteger.valueOf((Integer) modelMap.get(Constants.BUTTON_MODEL_VALUE));
		this.tuitionFeeViewList = (ObservableList<TuitionFeeView>) modelMap.get(Constants.TUITION_FEE_LIST_MODEL_DATA_TAG);
		this.tuitionFeeTable = (TableView<TuitionFeeView>) modelMap.get(Constants.TUITION_FEE_TABLE_MODEL_DATA_TAG);
	}

	@FXML
	public void loadInvoiceFile() {
		FileChooser fc = new FileChooser();
		File file = fc.showOpenDialog(this.getScreen());
		if (file != null) {
			try {
				lblFileName.setText(file.getName());
				TuitionFee tf = tuitionFeeService
						.setFilenameForPersonTuitionFee(
								file.getCanonicalPath(), tuitionFeeID);
				tuitionFeeViewList.stream()
						.filter(p -> p.getId().equals(tuitionFeeID.intValue()))
						.forEach(p -> p.setStatus(tf.getStatus()));
			} catch (IOException e) {
				System.out.println("Greška sa datotekom");
				e.printStackTrace();
			}
		}
	}

}
