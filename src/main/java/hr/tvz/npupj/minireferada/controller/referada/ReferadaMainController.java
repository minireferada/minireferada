package hr.tvz.npupj.minireferada.controller.referada;

import hr.tvz.npupj.minireferada.baza.DatabaseUtils;
import hr.tvz.npupj.minireferada.component.TableColumnButtonCell;
import hr.tvz.npupj.minireferada.component.TableColumnButtonCell.ButtonConfig;
import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import hr.tvz.npupj.minireferada.model.Kolegij;
import hr.tvz.npupj.minireferada.model.Semestar;
import hr.tvz.npupj.minireferada.model.TuitionFeeView;
import hr.tvz.npupj.minireferada.model.UnosKolegija;
import hr.tvz.npupj.minireferada.model.WorkersRoles;
import hr.tvz.npupj.minireferada.model.WorkersTvz;
import hr.tvz.npupj.minireferada.service.TuitionFeeService;
import hr.tvz.npupj.minireferada.service.WorkersService;
import hr.tvz.npupj.minireferada.service.WorkersTvzService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class ReferadaMainController extends AbstractScreenController {

    @Autowired
    private TuitionFeeService                    tuitionFeeService;

    @FXML
    private TableView<TuitionFeeView>            tbLoadedInvoice;

    /* Columns */
    @FXML
    private TableColumn<TuitionFeeView, String>  columnFeeType;
    @FXML
    private TableColumn<TuitionFeeView, String>  columnDueDate;
    @FXML
    private TableColumn<TuitionFeeView, Double>  columnAmount;
    @FXML
    private TableColumn<TuitionFeeView, String>  columnDescription;
    @FXML
    private TableColumn<TuitionFeeView, Double>  columnPaidAmount;
    @FXML
    private TableColumn<TuitionFeeView, String>  columnStatus;
    @FXML
    private TableColumn<TuitionFeeView, Integer> columnCheckInvoice;

    private ArrayList<Semestar>                  semestri;
    @FXML
    private TextField                            TextFieldUpisiNazivKolegija;
    @FXML
    private ComboBox<Semestar>                   ComboBoxSemestar;
    @FXML
    private ComboBox<String>                     ComboBoxObavezni;
    @FXML
    private TableColumn<UnosKolegija, String>    ColumnNazivKolegija;
    @FXML
    private TableColumn<UnosKolegija, String>    ColumnObavezni;
    @FXML
    private TableView<Kolegij>                   TableKolegija;

    private Semestar                             odabraniSemestar;

    @FXML
    private TextField                            txtWorkerName;

    @FXML
    private TextField                            txtWorkerLastName;

    @FXML
    private DatePicker                           dateOfBirth;

    @FXML
    private TextField                            txtOIBField;

    @FXML
    private TableView<WorkersTvz>                tableOfWorkers;

    @FXML
    private TableColumn<WorkersTvz, String>      tblColumnName;

    @FXML
    private TableColumn<WorkersTvz, String>      tblColumnLastName;

    @FXML
    private TableColumn<WorkersTvz, String>      tblColumnWorkplace;

    @FXML
    private ComboBox<WorkersRoles>               comboWorkers;

    @Autowired
    private WorkersService                       workersService;

    @Autowired
    private WorkersTvzService                    serviceWorkersTvz;

    @Override
    public void onShow(ModelMap modelMap) {
        super.onShow(modelMap);
        columnFeeType.setCellValueFactory(new PropertyValueFactory<TuitionFeeView, String>("tuitionType"));
        columnDueDate.setCellValueFactory(new PropertyValueFactory<TuitionFeeView, String>("dueDate"));
        columnAmount.setCellValueFactory(new PropertyValueFactory<TuitionFeeView, Double>("feeAmount"));
        columnDescription.setCellValueFactory(new PropertyValueFactory<TuitionFeeView, String>("description"));
        columnPaidAmount.setCellValueFactory(new PropertyValueFactory<TuitionFeeView, Double>("paidAmount"));
        columnStatus.setCellValueFactory(new PropertyValueFactory<TuitionFeeView, String>("status"));
        columnCheckInvoice.setCellValueFactory(new PropertyValueFactory<TuitionFeeView, Integer>("filepath"));

        ButtonConfig btnGenCfg = new ButtonConfig("Uplatnica", (event, dataMap) -> {
            showModal("invoiceImageScreen", dataMap);
        });

        columnCheckInvoice.setCellFactory(new TableColumnButtonCell<TuitionFeeView, Integer>(btnGenCfg));
        List<TuitionFeeView> loadedInvoices = tuitionFeeService.getAllLoadedInvoices();
        tbLoadedInvoice.setItems(FXCollections.observableArrayList(loadedInvoices));
        popuniComboBoxObavezni();
    }

    @FXML
    public void newInvoice() {
        showModal("newInvoiceScreen");
    }

    public void populateTable(List<Kolegij> kolegiji) {

        ObservableList<Kolegij> data = FXCollections.observableArrayList(kolegiji);
        ColumnNazivKolegija.setCellValueFactory(new PropertyValueFactory<UnosKolegija, String>("naziv"));
        ColumnObavezni.setCellValueFactory(new PropertyValueFactory<UnosKolegija, String>("obveznost"));

        TableKolegija.setItems(data);
    }

    @FXML
    public void dodajKolegij() {

        Semestar semestar = ComboBoxSemestar.getSelectionModel().getSelectedItem();
        Kolegij kolegij = new Kolegij();
        kolegij.setNaziv(TextFieldUpisiNazivKolegija.getText());

        DatabaseUtils.spremiPodatke(kolegij);
        ComboBoxSemestar.getSelectionModel().select(odabraniSemestar);
        odabraniSemestar.getKolegiji().add(kolegij);
        populateTable(odabraniSemestar.getKolegiji());
    }

    @FXML
    public void popuniComboBoxObavezni() {

        if (ComboBoxObavezni.getSelectionModel().isEmpty()) {
            ComboBoxObavezni.getItems().addAll("DA", "NE");
        }
    }

    @FXML
    public void popuniComboBoxSemestar() {
        semestri = DatabaseUtils.dohvatiSemestreKolegija();
        ComboBoxSemestar.setItems(FXCollections.observableArrayList(semestri));
    }

    @FXML
    public void promijeniKolegije() {
        Semestar odabraniSemestar = semestri.get(ComboBoxSemestar.getSelectionModel().getSelectedIndex());
        populateTable(odabraniSemestar.getKolegiji());
    }

    @FXML
    public void obrisiKolegij() {
        Kolegij kolegij = TableKolegija.getSelectionModel().getSelectedItem();
        DatabaseUtils.obrisiPodatke(kolegij);
        ComboBoxSemestar.getSelectionModel().select(odabraniSemestar);
        odabraniSemestar.getKolegiji().remove(kolegij);
        populateTable(odabraniSemestar.getKolegiji());
    }

    @FXML
    public void urediKolegij() {

        Kolegij kolegij = TableKolegija.getSelectionModel().getSelectedItem();
        DatabaseUtils.urediPodatke(kolegij);
        ComboBoxSemestar.getSelectionModel().select(odabraniSemestar);
        populateTable(odabraniSemestar.getKolegiji());
    }

    @FXML
    public void performLogout(ActionEvent actionEvent) {
        switchTo("loginScreen");
    }

    @FXML
    public void fillComboWorkers() {

        List<WorkersRoles> workers = workersService.getAllWorkers();
        comboWorkers.setItems(FXCollections.observableArrayList(workers));

    }

    public void populateTableWorkers() {

        ObservableList<WorkersTvz> data = FXCollections.observableList(serviceWorkersTvz.getAllWorkersTvz());

        tblColumnName.setCellValueFactory(new PropertyValueFactory<WorkersTvz, String>("workerName"));
        tblColumnLastName.setCellValueFactory(new PropertyValueFactory<WorkersTvz, String>("workerLastName"));
        tblColumnWorkplace.setCellValueFactory(new PropertyValueFactory<WorkersTvz, String>("workerRoles"));

        tableOfWorkers.setItems(data);

    }

    @FXML
    public void saveWorkers() {

        String name = txtWorkerName.getText();
        String lastName = txtWorkerLastName.getText();
        String OIB = txtOIBField.getText();

        LocalDate datum = dateOfBirth.getValue();
        WorkersRoles workerRole = comboWorkers.getSelectionModel().getSelectedItem();

        WorkersTvz worker = new WorkersTvz(name, lastName, Date.valueOf(datum), OIB, workerRole);

        serviceWorkersTvz.addNewWorker(worker);
        populateTableWorkers();

    }

    @FXML
    public void updateWorker() {
        populateTableWorkers();
    }

    @FXML
    public void showNewStudentEntryScreen(ActionEvent event) {
        showModal("referadaNewStudentEntryScreen");
    }

    @FXML
    public void showEditStudentDataEntryScreen(ActionEvent event) {
        showModal("referadaEditStudentEntryScreen");
    }

    @FXML
    public void showDeleteStudentDataEntryScreen(ActionEvent event) {
        showModal("referadaDeleteStudentEntryScreen");
    }

}
