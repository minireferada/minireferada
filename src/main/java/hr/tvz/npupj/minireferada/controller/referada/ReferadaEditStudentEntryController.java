package hr.tvz.npupj.minireferada.controller.referada;

import java.math.BigInteger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import hr.tvz.npupj.minireferada.config.UserRole;
import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import hr.tvz.npupj.minireferada.controller.common.ValidationResult;
import hr.tvz.npupj.minireferada.controller.student.StudentDataController;
import hr.tvz.npupj.minireferada.dao.PersonRepository;
import hr.tvz.npupj.minireferada.model.Osoba;
import hr.tvz.npupj.minireferada.model.Student;
import hr.tvz.npupj.minireferada.model.StudentAdmission;
import hr.tvz.npupj.minireferada.service.StudentAdmissionService;
import hr.tvz.npupj.minireferada.service.StudentService;
import hr.tvz.npupj.minireferada.util.FxModelMapper;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

@Controller
public class ReferadaEditStudentEntryController extends AbstractScreenController {

	
    private static final Logger LOGGER = LoggerFactory.getLogger(StudentDataController.class);
    
    @Autowired
    private StudentService      studentService;
    
    @Autowired
    private StudentAdmissionService      studentAdmissionService;
    
    
    @Autowired
    private PersonRepository     personRepository;
    
    @FXML
    private TextField       txtFindByJMBAG;
    
    @FXML
    private TextField       txtStudentName;
    
    @FXML
    private TextField       txtStudentLastname;
    
    @FXML
    private TextField       txtStudentBirthDate;
    
    @FXML
    private TextField       txtStudentAdress;
    
    @FXML
    private TextField       txtStudentCity;
    
    @FXML
    private TextField       txtStudentPostNumber;
    
    @FXML
    private TextField       txtStudentOib;
    
    @FXML
    private TextField       txtStudentJmbag;
    
    @FXML
    private TextField       txtStudentEmail;
    
    @FXML
    private TextField       txtStudentPhone;
    
    @FXML
    private TextField       txtAcademicYear;
    
    @FXML
    private TextField       txtTeachingYear;
    
    @FXML
    private TextField       txtSemester;
    
    @FXML
    private TextField       txtStudy;
    
    @FXML
    private TextField       txtAdmissionDate;
    
    @FXML
    private TextField       txtStudyCurse;
    
    @FXML
    private TextField       txtAdmissionMethod;
    
    @FXML
    private TextField       txtTuitionFee;
    
    @FXML
    private TextField       txtEtscPrice;
    
    @FXML
    private TextField       txtRights;
    
    @FXML
    private TextField       txtUserName;
    
    @FXML
    private TextField       txtPassword;

    
    
    @FXML
    private Label       lblVerificationJMBAG;
    
    @FXML
   	public void showStudentData(ActionEvent event) {
    	
    		ModelMap modelMap = new ModelMap();
    		StudentAdmission studentAdmission = studentAdmissionService.getStudentAdmissionById( BigInteger.valueOf(1) ); 
    		modelMap.addAttribute("studentAdmission", studentAdmission);
    	
    		Student student = studentService.getStudentByJmbag(txtFindByJMBAG.getText());
    		modelMap.addAttribute("student", student);
    		
    	if (student != null && txtFindByJMBAG.getText().length() == 11 ){
    		
    		lblVerificationJMBAG.setText("");
    		txtStudentName.setText(student.getFirstname());
    		txtStudentLastname.setText(student.getLastname());
    		txtStudentBirthDate.setText(student.getBirthDate());
    		txtStudentAdress.setText(student.getAddress());
    		txtStudentCity.setText(student.getCity());
    		txtStudentPostNumber.setText(student.getPostNumber());
    		txtStudentOib.setText(student.getOib());
    		txtStudentJmbag.setText(student.getJmbag());
    		txtStudentEmail.setText(student.getEmail());
    		txtStudentPhone.setText(student.getTelNumber());
    		txtUserName.setText(student.getUser().getName());
    		txtPassword.setText(student.getUser().getPassword());
    		
    		txtAcademicYear.setText(studentAdmission.getAcademicYear());
    		txtTeachingYear.setText(studentAdmission.getTeachingYear());
    		txtSemester.setText(studentAdmission.getSemester());
    		txtStudy.setText(studentAdmission.getStudy());
    		txtStudyCurse.setText(studentAdmission.getStudyCurse());
    		txtAdmissionDate.setText(studentAdmission.getAdmissionDate());
    		txtAdmissionMethod.setText(studentAdmission.getAdmissionMethod());
    		txtTuitionFee.setText(studentAdmission.getTuitionFee());
    		txtEtscPrice.setText(studentAdmission.getEtscPrice());
    		txtRights.setText(studentAdmission.getRights());
    		
    	}  else if (student == null && txtFindByJMBAG.getText().length() == 11 ) {
    		
    		txtFindByJMBAG.setText("");
    		lblVerificationJMBAG.setText("Takav student ne postoji!");
    		
    	}else {
    		
    		lblVerificationJMBAG.setText("Niste unijeli ispravan JMBAG");
    		txtFindByJMBAG.setText("KRIVI JMBAG");
    	}
    	
   	}
	
    
    
    @FXML
   	public void editStudent() {
		ValidationResult vr = validateForm();
		if (vr.isValid()) {
			Student editData = fillData();
			
			studentService.editStudent(editData);
		} else {
			showMessageScreen(vr.getMessage());
		}
    }
    
    
    
    @FXML
   	public void clearAllEntries(ActionEvent event) {
    	txtFindByJMBAG.setText("");
		lblVerificationJMBAG.setText("* Unesite Jmbag od studenta!");
		txtStudentName.setText("");
		txtStudentLastname.setText("");
		txtStudentBirthDate.setText("");
		txtStudentAdress.setText("");
		txtStudentCity.setText("");
		txtStudentPostNumber.setText("");
		txtStudentOib.setText("");
		txtStudentJmbag.setText("");
		txtStudentEmail.setText("");
		txtStudentPhone.setText("");
		
		
		txtAcademicYear.setText("");
		txtTeachingYear.setText("");
		txtSemester.setText("");
		txtStudy.setText("");
		txtStudyCurse.setText("");
		txtAdmissionDate.setText("");
		txtAdmissionMethod.setText("");
		txtTuitionFee.setText("");
		txtEtscPrice.setText("");
		txtRights.setText("");
    }
    
    private ValidationResult validateForm() {
		ValidationResult vr = new ValidationResult(true, null);
		if (txtStudentName.getText() == null || txtStudentName.getText().isEmpty()) {
			vr.setValid(false);
			vr.addMessage("Upišite ime studenta");
		}
		if (txtStudentLastname.getText() == null || txtStudentLastname.getText().isEmpty()) {
			vr.setValid(false);
			vr.addMessage("Upišite prezime studenta");
		}
		if (txtStudentBirthDate.getText() == null || txtStudentBirthDate.getText().isEmpty()) {
			vr.setValid(false);
			vr.addMessage("Upišite datum rođenja studenta");
		}
		if (txtStudentAdress.getText() == null || txtStudentAdress.getText().isEmpty()) {
			vr.setValid(false);
			vr.addMessage("Upišite adresu studenta");
		}
		if (txtStudentCity.getText() == null || txtStudentCity.getText().isEmpty()) {
			vr.setValid(false);
			vr.addMessage("Upišite grad studenta");
		}
		if (txtStudentPostNumber.getText() == null || txtStudentPostNumber.getText().isEmpty()) {
			vr.setValid(false);
			vr.addMessage("Upišite broj pošte studenta");
		}
		if (txtStudentOib.getText() == null || txtStudentOib.getText().isEmpty()) {
			vr.setValid(false);
			vr.addMessage("Upišite oib studenta");
		}
		if (txtStudentJmbag.getText() == null || txtStudentJmbag.getText().isEmpty()) {
			vr.setValid(false);
			vr.addMessage("Upišite jmbag studenta");
		}
		if (txtStudentEmail.getText() == null || txtStudentEmail.getText().isEmpty()) {
			vr.setValid(false);
			vr.addMessage("Upišite email studenta");
		}
		if (txtStudentPhone.getText() == null || txtStudentPhone.getText().isEmpty()) {
			vr.setValid(false);
			vr.addMessage("Upišite telefonski broj  studenta");
		}
		if (txtUserName.getText() == null || txtUserName.getText().isEmpty()) {
			vr.setValid(false);
			vr.addMessage("Upišite telefonski broj  studenta");
		}
		if (txtPassword.getText() == null || txtPassword.getText().isEmpty()) {
			vr.setValid(false);
			vr.addMessage("Upišite telefonski broj  studenta");
		}

		return vr;
	}

    
    
	private Student fillData() {
		Student StudentData = studentService.getStudentByJmbag(txtFindByJMBAG.getText());
		Osoba student = new Osoba();
		
	    student.setName(txtUserName.getText());
	    student.setPassword(DigestUtils.shaHex(txtPassword.getText())); 
	    student.setRole(UserRole.ROLE_STUDENT);
	    student.setEnabled(1);
	    Osoba persistedUser = personRepository.save(student);
	    StudentData.setUser(persistedUser); 
	        
		
		StudentData.setFirstname(txtStudentName.getText());
		StudentData.setLastname(txtStudentLastname.getText());
		StudentData.setBirthDate(txtStudentBirthDate.getText());
		StudentData.setAddress(txtStudentAdress.getText());
		StudentData.setCity(txtStudentCity.getText());
		StudentData.setPostNumber(txtStudentPostNumber.getText());
		StudentData.setOib(txtStudentOib.getText());
		StudentData.setJmbag(txtStudentJmbag.getText());
		StudentData.setEmail(txtStudentEmail.getText());
		StudentData.setTelNumber(txtStudentPhone.getText());
		
		return StudentData;
	}
	
	
    @Override
    public void onShow(ModelMap p_modelMap) {

    }

	
}
