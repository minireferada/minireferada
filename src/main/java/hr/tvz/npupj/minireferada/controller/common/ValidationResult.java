package hr.tvz.npupj.minireferada.controller.common;

public class ValidationResult {

	private boolean valid;
	private StringBuilder messageBuilder;

	public ValidationResult() {
		valid = false;
		messageBuilder = new StringBuilder();
	}

	public ValidationResult(boolean valid, String msg) {
		this.valid = valid;
		this.messageBuilder = new StringBuilder();
		addMessage(msg);
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getMessage() {
		return messageBuilder.toString();
	}

	public void addMessage(String message) {
		if (message != null && !message.isEmpty()) {
			messageBuilder.append(message);
			messageBuilder.append("\n");
		}
	}

}
