package hr.tvz.npupj.minireferada.controller.student;

import hr.tvz.npupj.minireferada.common.Constants;
import hr.tvz.npupj.minireferada.common.DataCollector;
import hr.tvz.npupj.minireferada.component.TableColumnButtonCell;
import hr.tvz.npupj.minireferada.component.TableColumnButtonCell.ButtonConfig;
import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import hr.tvz.npupj.minireferada.model.TuitionFeeView;
import hr.tvz.npupj.minireferada.service.TuitionFeeService;
import hr.tvz.npupj.minireferada.util.DateUtils;

import java.util.Date;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

/**
 * Created by mjovanovic on 26/05/15.
 */
@Component
public class StudentTuitionFeesController extends AbstractScreenController {
	@Autowired
	TuitionFeeService tuitionFeeService;
	
	@FXML
	TableView<TuitionFeeView> tuitionsFeeTable;
	
	/*Columns*/
	@FXML
	TableColumn<TuitionFeeView, String> columnFeeType;
	@FXML
	TableColumn<TuitionFeeView, String> columnDueDate;
	@FXML
	TableColumn<TuitionFeeView, Double> columnAmount;
	@FXML
	TableColumn<TuitionFeeView, String> columnDescription;
	@FXML
	TableColumn<TuitionFeeView, Double> columnPaidAmount;
	@FXML
	TableColumn<TuitionFeeView, String> columnStatus;
	@FXML
	TableColumn<TuitionFeeView, Integer> columnGenerateInvoice;
	@FXML
	TableColumn<TuitionFeeView, Integer> columnLoadInvoice;
	
	/*Student data*/
	@FXML
	Label personNameAndSurname;
	@FXML
	Label accountStatus; 
	
	/*View data*/
	@FXML
	Label lblCurrentDate;
	@FXML
	Label lblAccountStatusTip;
	@FXML
	Label lblCurrency;
	
	ObservableList<TuitionFeeView> tuitionFeeTableList = FXCollections.observableArrayList();
	
	@FXML
	public void showPersonDataScreen(ActionEvent event) {
		showModal("studentDataScreen");
	}
	
	@Override
	public void onShow(ModelMap modelMap) {
		if (tuitionFeeTableList.size() > 0) {
			tuitionFeeTableList.clear();
		}
		columnFeeType.setCellValueFactory(cellData -> cellData.getValue().getTuitionTypeProperty());
		columnDueDate.setCellValueFactory(new PropertyValueFactory<TuitionFeeView, String>("dueDate"));
		columnAmount.setCellValueFactory(new PropertyValueFactory<TuitionFeeView, Double>("feeAmount"));
		columnDescription.setCellValueFactory(new PropertyValueFactory<TuitionFeeView, String>("description"));
		columnPaidAmount.setCellValueFactory(new PropertyValueFactory<TuitionFeeView, Double>("paidAmount"));
		columnStatus.setCellValueFactory(cellData -> cellData.getValue().getStatusProperty());
		columnGenerateInvoice.setCellValueFactory(new PropertyValueFactory<TuitionFeeView, Integer>("id"));
		
		ButtonConfig btnGenCfg = new ButtonConfig("Uplatnica", (event, dataMap) -> {
			showModal("generateInvoiceScreen", dataMap);
		});
		
		columnGenerateInvoice.setCellFactory(new TableColumnButtonCell<TuitionFeeView, Integer>(btnGenCfg));
		
		columnLoadInvoice.setCellValueFactory(new PropertyValueFactory<TuitionFeeView, Integer>("id"));
		
		ButtonConfig btnLoadCfg = new ButtonConfig("Plaćanje", (event, dataMap) -> {
			showModal("loadInvoiceScreen", dataMap);
		});
		ModelMap loadInvoiceData = new ModelMap();
		loadInvoiceData.put(Constants.TUITION_FEE_LIST_MODEL_DATA_TAG, tuitionFeeTableList);
		loadInvoiceData.put(Constants.TUITION_FEE_TABLE_MODEL_DATA_TAG, tuitionsFeeTable);
		
		columnLoadInvoice.setCellFactory(new TableColumnButtonCell<TuitionFeeView, Integer>(btnLoadCfg, loadInvoiceData));
		tuitionFeeTableList.addAll(tuitionFeeService.getTuitionFeesForCurrentUser());
		tuitionsFeeTable.setItems(tuitionFeeTableList);
		personNameAndSurname.setText(DataCollector.getStudentData().toString());
		accountStatus.setText(tuitionFeeService.getTuitionFeeAmountForCurrentUser().toString());
		lblCurrency.setText(Constants.CURRENCY);
		lblAccountStatusTip.setText(String.format("%s %s", "*Stanje računa na dan", DateUtils.formatUiDate(new Date()) ));
		lblCurrentDate.setText(DateUtils.formatUiDate(new Date()));
		super.onShow(modelMap);
	}
	
	@Override
	public void onHide(ModelMap modelMap) {
		super.onHide(modelMap);
		this.tuitionFeeTableList.clear();
	}
}
