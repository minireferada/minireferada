package hr.tvz.npupj.minireferada.controller.referada;

import java.math.BigInteger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import hr.tvz.npupj.minireferada.controller.student.StudentDataController;
import hr.tvz.npupj.minireferada.model.Student;
import hr.tvz.npupj.minireferada.model.StudentAdmission;
import hr.tvz.npupj.minireferada.service.StudentAdmissionService;
import hr.tvz.npupj.minireferada.service.StudentService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
@Controller
public class ReferadaDeleteStudentEntryController extends AbstractScreenController {

	
    private static final Logger LOGGER = LoggerFactory.getLogger(StudentDataController.class);
    
    @Autowired
    private StudentService      studentService;
    
    @Autowired
    private StudentAdmissionService      studentAdmissionService;
    
    @FXML
    private TextField       txtFindByJMBAG;
	
    @FXML
    private TextField       txtStudentName;
    
    @FXML
    private TextField       txtStudentLastname;
    
    @FXML
    private TextField       txtStudentOib;
    
    @FXML
    private TextField       txtAcademicYear;
    
    @FXML
    private TextField      txtStudyCurse;
     
    @FXML
    private TextField       txtStudy;

    
    @FXML
    private Label       lblVerificationJMBAG;
    
    @FXML
   	public void showStudentData(ActionEvent event) {
    	
    		ModelMap modelMap = new ModelMap();
    		StudentAdmission studentAdmission = studentAdmissionService.getStudentAdmissionById( BigInteger.valueOf(1) ); 
    		modelMap.addAttribute("studentAdmission", studentAdmission);
    	
    		Student student = studentService.getStudentByJmbag(txtFindByJMBAG.getText());
    		modelMap.addAttribute("student", student);
    		
    	if (student != null){
    		
    		lblVerificationJMBAG.setText("");
    		txtStudentName.setText(student.getFirstname());
    		txtStudentLastname.setText(student.getLastname());
    		txtStudentOib.setText(student.getOib());
    		txtAcademicYear.setText(studentAdmission.getAcademicYear());
    		txtStudyCurse.setText(studentAdmission.getStudyCurse());
    		txtStudy.setText(studentAdmission.getStudy());

    	} else {
    		
    		lblVerificationJMBAG.setText("Niste unijeli ispravan JMBAG");
    		txtFindByJMBAG.setText("KRIVI JMBAG");
    	}
    	
   	}
    
    @FXML
   	public void deleteStudentData(ActionEvent event) {

		Student student = studentService.getStudentByJmbag(txtFindByJMBAG.getText());
		studentService.deleteStudent( student );
		
		lblVerificationJMBAG.setText("* Obrisali ste studenta!");
		txtStudentName.setText("");
		txtStudentLastname.setText("");
		txtStudentOib.setText("");
		txtAcademicYear.setText("");
		txtStudy.setText("");
		txtStudyCurse.setText("");
		txtFindByJMBAG.setText("");

    	
    }
    
    @FXML
   	public void clearAllEntries(ActionEvent event) {
    	txtFindByJMBAG.setText("");
		lblVerificationJMBAG.setText("* Unesite Jmbag od studenta!");
		txtStudentName.setText("");
		txtStudentLastname.setText("");
		txtStudentOib.setText("");
		txtAcademicYear.setText("");
		txtStudy.setText("");
		txtStudyCurse.setText("");

    }
    
	@Override
	public void onShow(ModelMap modelMap) {

	}

	
}
