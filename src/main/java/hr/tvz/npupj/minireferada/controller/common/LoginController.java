package hr.tvz.npupj.minireferada.controller.common;

import com.google.common.base.Optional;

import java.util.Collection;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import hr.tvz.npupj.minireferada.common.DataCollector;
import hr.tvz.npupj.minireferada.config.UserRole;
import hr.tvz.npupj.minireferada.model.Osoba;
import hr.tvz.npupj.minireferada.model.Student;
import hr.tvz.npupj.minireferada.service.LoginService;

@Component
public class LoginController extends AbstractScreenController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private LoginService        loginService;

    @FXML
    private Label               lblMessage;
    @FXML
    private TextField           txtUsername;
    @FXML
    private PasswordField       txtPassword;

    @FXML
    public void btnLoginAction(ActionEvent event) {
        LOGGER.info("Performing login");

        Optional<Authentication> authToken = loginService.performeLogin(txtUsername.getText(), txtPassword.getText());

        if (authToken.isPresent()) {
            Authentication authentication = authToken.get();
            Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

            if (authorities.contains(new SimpleGrantedAuthority(UserRole.ROLE_STUDENT))) {
                Osoba user = loginService.getUserData(txtUsername.getText());
                Student studentData = loginService.getStudentData(user);
                DataCollector.setUser(user);
                DataCollector.setStudentData(studentData);
                switchTo("studentMainScreen");
            } else if (authorities.contains(new SimpleGrantedAuthority(UserRole.ROLE_PROFESOR))) {
                switchTo("professorMainScreen");
            } else if (authorities.contains(new SimpleGrantedAuthority(UserRole.ROLE_REFERADA))) {
                switchTo("referadaMainScreen");
            } else if (authorities.contains(new SimpleGrantedAuthority(UserRole.ROLE_ADMIN))) {
                switchTo("adminMainScreen");
            }

        } else {
            lblMessage.setText("Unjeli ste krive podatke!");
        }

    }
}
