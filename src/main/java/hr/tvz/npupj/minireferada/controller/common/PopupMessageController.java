package hr.tvz.npupj.minireferada.controller.common;

import hr.tvz.npupj.minireferada.common.Constants;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

@Controller
public class PopupMessageController extends AbstractScreenController{
	
	@FXML
	private TextArea message;
	
	@Override
	public void onShow(ModelMap modelMap) {
		String msg = (String) modelMap.get(Constants.POPUP_MSG_TEXT_TAG);
		if (msg != null && !msg.isEmpty()) {
			message.setText(msg);
		} else {
			message.setText("Nema poruke");
		}
		super.onShow(modelMap);
	}
	
	@FXML
	public void closeWindow() {
		this.getScreen().hide();
	}
}
