package hr.tvz.npupj.minireferada.controller.common;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;

public class ButtonCell<T, S> extends TableCell<T, S> {

	private Button cellButton;

	public ButtonCell(String title, String screenName,
			AbstractScreenController sceneController) {
		cellButton = new Button(title);
		cellButton.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				sceneController.showModal(screenName);
			}
		});
	}

	@Override
	protected void updateItem(S arg0, boolean arg1) {
		super.updateItem(arg0, arg1);
		if (arg0 != null) {
			setGraphic(cellButton);
		}
	}

}
