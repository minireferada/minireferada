package hr.tvz.npupj.minireferada.controller.common;

import hr.tvz.npupj.minireferada.common.Constants;
import hr.tvz.npupj.minireferada.config.FxmlScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.ui.ModelMap;

/**
 * Created by mjovanovic on 19/05/15.
 */
public abstract class AbstractScreenController implements
		ApplicationContextAware {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AbstractScreenController.class);

	private ApplicationContext appCtx;
	private FxmlScreen screen;

	@FXML
	public void performLogout(ActionEvent event) {
		LOGGER.info("Logging out..");
	}

	// Lifecycle methods

	/**
	 * This method is called inside constructor
	 */
	public void onCreate() {
	}

	/**
	 * This method is called before {@link Stage#show()}
	 */
	public void onShow(ModelMap modelMap) {
	}

	/**
	 * This method is called before {@link Stage#hide()}
	 */
	public void onHide(ModelMap modelMap) {
	}

	protected void switchTo(String screenName) {
		switchTo(screenName, null);
	}

	protected void switchTo(String screenName, ModelMap p_modelMap) {
		FxmlScreen screen = appCtx.getBean(screenName, FxmlScreen.class);
		screen.hideAndShow(getScreen(), p_modelMap);
	}

	protected void showModal(String screenName) {
		showModal(screenName, null);
	}

	protected void showModal(String screenName, ModelMap p_modelMap) {
		FxmlScreen screen = appCtx.getBean(screenName, FxmlScreen.class);
		screen.openModal(getScreen(), p_modelMap);
	}

	// Getters // Setters

	public FxmlScreen getScreen() {
		return screen;
	}

	public void setScreen(FxmlScreen screen) {
		this.screen = screen;
	}

	@Override
	public void setApplicationContext(ApplicationContext appCtx)
			throws BeansException {
		this.appCtx = appCtx;
	}

	public void showMessageScreen(String msg) {
		ModelMap modelMap = new ModelMap();
		modelMap.put(Constants.POPUP_MSG_TEXT_TAG, msg);
		showModal("popupMessageScreen", modelMap);
	}
}
