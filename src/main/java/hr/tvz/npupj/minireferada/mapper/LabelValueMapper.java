package hr.tvz.npupj.minireferada.mapper;

import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import javafx.scene.control.Label;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;

/**
 * Created by mjovanovic on 30/05/15.
 */
@Component
public final class LabelValueMapper implements ValueMapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(LabelValueMapper.class);

    @Override
    public boolean supports(Class<?> type) {
        return Label.class.isAssignableFrom(type);
    }

    public void map(Object model, Field modelField, AbstractScreenController controller) {

        try {
            mapInternal(model, modelField, controller);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            LOGGER.warn("Fail to map Label value", e);
        }

    }

    @Override
    public String getPrefix() {
        return "lbl";
    }

    private void mapInternal(Object model, Field modelField, AbstractScreenController controller) throws NoSuchFieldException, IllegalAccessException {
        String modelFieldName = modelField.getName();
        String targetFieldName = getPrefix() + StringUtils.capitalize(modelFieldName);

        Field targetField = controller.getClass().getDeclaredField(targetFieldName);

        modelField.setAccessible(true);
        targetField.setAccessible(true);

        Object modelValue = modelField.get(model);
        Label label = (Label) targetField.get(controller);

        label.setText((String) modelValue);
    }
}
