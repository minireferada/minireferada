package hr.tvz.npupj.minireferada.mapper;

import java.lang.reflect.Field;

import javafx.scene.control.TextField;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;

/**
 * Created by mjovanovic on 30/05/15.
 */
@Component
public final class TextfieldValueMapper implements ValueMapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(TextfieldValueMapper.class);

    @Override
    public boolean supports(Class<?> type) {
        return TextField.class.isAssignableFrom(type);
    }

    public void map(Object model, Field modelField, AbstractScreenController controller) {

        try {
            mapInternal(model, modelField, controller);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            LOGGER.warn("Fail to map TextField value", e);
        }

    }

    @Override
    public String getPrefix() {
        return "txt";
    }

    private void mapInternal(Object model, Field modelField, AbstractScreenController controller) throws NoSuchFieldException, IllegalAccessException {
        String modelFieldName = modelField.getName();
        String targetFieldName = getPrefix() + StringUtils.capitalize(modelFieldName);

        Field targetField = controller.getClass().getDeclaredField(targetFieldName);

        modelField.setAccessible(true);
        targetField.setAccessible(true);

        Object modelValue = modelField.get(model);
        TextField textField = (TextField) targetField.get(controller);

        textField.setText((String) modelValue);
    }
}
