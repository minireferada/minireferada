package hr.tvz.npupj.minireferada.mapper;

import java.lang.reflect.Field;

import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;

/**
 * Created by mjovanovic on 30/05/15.
 */
public interface ValueMapper {

    boolean supports(Class<?> type);

    void map(Object model, Field modelField, AbstractScreenController controller);

    String getPrefix();
}
