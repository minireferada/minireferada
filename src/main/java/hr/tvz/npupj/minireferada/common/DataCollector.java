package hr.tvz.npupj.minireferada.common;

import hr.tvz.npupj.minireferada.model.Osoba;
import hr.tvz.npupj.minireferada.model.Student;
import hr.tvz.npupj.minireferada.model.TuitionFeeView;

import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DataCollector {
	public static ObservableList<TuitionFeeView> tuitionFeeViewItems;
	public static Osoba user;
	public static Student student;
	
	public static ObservableList<TuitionFeeView> getTuitionFeeViewItems() {
		return tuitionFeeViewItems;
	}

	public static void setTuitionFeeViewItems(
			List<TuitionFeeView> tuitionFeeViewItems) {
		DataCollector.tuitionFeeViewItems = FXCollections.observableArrayList(tuitionFeeViewItems);
	}

	public static ObservableList<String> getFeeStatusList() {
		ObservableList<String> feeTypeLs = FXCollections.observableArrayList();
		feeTypeLs.add("DODJELA");
		feeTypeLs.add("PREDANO");
		feeTypeLs.add("POTVRĐENO");
		feeTypeLs.add("GREŠKA");
		return feeTypeLs;
	}
	
	public static void setUser(Osoba user) {
		DataCollector.user = user;
	}
	
	public static Osoba getUser() {
		return user;
	}

	public static void setStudentData(Student studentData) {
		DataCollector.student = studentData;
	}
	
	public static Student getStudentData() {
		return student;
	}

	public static ObservableList<String> getFeeTypeList() {
		ObservableList<String> feeTypeLs = FXCollections.observableArrayList();
		feeTypeLs.add("SKOLARINA");
		feeTypeLs.add("UPISNINA");
		feeTypeLs.add("TROSKOVI");
		return feeTypeLs;
	}
	
}
