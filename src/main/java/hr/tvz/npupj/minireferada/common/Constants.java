package hr.tvz.npupj.minireferada.common;

public class Constants {
	public static final String CURRENCY = "HRK";
	public static final String BUTTON_MODEL_VALUE = "cellValue";
	public static final String TF_STATUS_PREDANO = "PREDANO";
	public static final String TUITION_FEE_LIST_MODEL_DATA_TAG = "TUITION_FEE_LIST_MODEL_DATA_TAG";
	public static final String TUITION_FEE_TABLE_MODEL_DATA_TAG = "TUITION_FEE_TABLE_MODEL_DATA_TAG";
	public static final String POPUP_MSG_TEXT_TAG = "POPUP_MSG_TEXT_TAG";
	public static final String LOADED_INVOICE_LIST_MODEL_DATA_TAG = "LOADED_INVOICE_LIST_MODEL_DATA_TAG";
}
