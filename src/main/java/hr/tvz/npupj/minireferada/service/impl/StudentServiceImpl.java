package hr.tvz.npupj.minireferada.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.tvz.npupj.minireferada.dao.StudentDataRepository;
import hr.tvz.npupj.minireferada.model.Student;
import hr.tvz.npupj.minireferada.service.StudentService;


@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDataRepository repository;
    
	@Autowired
	private StudentDataRepository studentDataRepository;

    @Override
    public List<Student> getAllStudents() {
        return repository.findAll();
    }

    @Override
    public Student getStudentByJmbag(String jmbag) {
        return repository.findByJmbag(jmbag);
    }
    
	@Override
	public Student addNewStudent (Student newStudentData) {
		return repository.saveAndFlush(newStudentData);
	}
	
	@Override
	public Student editStudent (Student editStudentData) {
		return repository.saveAndFlush(editStudentData);
	}
	   @Override
	   public void deleteStudent(Student student) {
	       repository.delete(student);
	   } 
}
