package hr.tvz.npupj.minireferada.service.impl;

import hr.tvz.npupj.minireferada.dao.WorkersRepository;
import hr.tvz.npupj.minireferada.model.WorkersRoles;
import hr.tvz.npupj.minireferada.service.WorkersService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class WorkersServiceImpl implements WorkersService {
	
	@Autowired
	private WorkersRepository repository;

	@Override
	public List<WorkersRoles> getAllWorkers() {
		return repository.findAll();
	}

}
