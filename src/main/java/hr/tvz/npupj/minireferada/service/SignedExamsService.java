package hr.tvz.npupj.minireferada.service;

import hr.tvz.npupj.minireferada.model.SignedExams;

import java.util.List;

/**
 * Created by mjovanovic on 29/05/15.
 */
public interface SignedExamsService {

    List<SignedExams> getAllSignedExams();
}
