package hr.tvz.npupj.minireferada.service;

import hr.tvz.npupj.minireferada.model.WorkersTvz;

import java.util.List;

public interface WorkersTvzService {

	public List<WorkersTvz> getAllWorkersTvz();
	
	WorkersTvz addNewWorker(WorkersTvz worker);
}
