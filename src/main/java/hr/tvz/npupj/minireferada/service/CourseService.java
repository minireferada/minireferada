package hr.tvz.npupj.minireferada.service;

import hr.tvz.npupj.minireferada.model.Course;

import java.util.List;

/**
 * Created by mjovanovic on 29/05/15.
 */
public interface CourseService {

    List<Course> getAllCourses();

    Course addNewCourse(Course p_course);

    Course getCourseByName(String p_name);
}
