package hr.tvz.npupj.minireferada.service;

import java.util.List;

import hr.tvz.npupj.minireferada.model.Student;

/**
 * Created by mjovanovic on 29/05/15.
 */
public interface StudentService {

    List<Student> getAllStudents();

    Student getStudentByJmbag(String jmbag);

    Student addNewStudent(Student newStudentData);
    
    void deleteStudent(Student student);
    
    Student editStudent(Student editStudentData);
}
