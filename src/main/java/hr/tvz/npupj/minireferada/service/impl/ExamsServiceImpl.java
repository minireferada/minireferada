package hr.tvz.npupj.minireferada.service.impl;

import hr.tvz.npupj.minireferada.dao.ExamsRepository;
import hr.tvz.npupj.minireferada.model.Exams;
import hr.tvz.npupj.minireferada.service.ExamsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mjovanovic on 29/05/15.
 */
@Service
public class ExamsServiceImpl implements ExamsService {

    @Autowired
    private ExamsRepository repository;

    @Override
    public List<Exams> getAllExams() {
        return repository.findAll();
    }
}
