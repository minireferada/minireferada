package hr.tvz.npupj.minireferada.service;

import hr.tvz.npupj.minireferada.model.Osoba;
import hr.tvz.npupj.minireferada.model.Student;
import hr.tvz.npupj.minireferada.model.TuitionFee;
import hr.tvz.npupj.minireferada.model.TuitionFeeView;

import java.math.BigInteger;
import java.util.List;

import javafx.collections.ObservableList;

public interface TuitionFeeService {

	ObservableList<TuitionFeeView> getTuitionFeesForStudent(Student student);

	Osoba getPersonForUsername(String username);

	BigInteger getTuitionFeeAmountForPerson(Student student);

	TuitionFee getTuitionFeeById(Integer cellValue);
	
	TuitionFee setFilenameForPersonTuitionFee(String filename, BigInteger tuitionFeeID);

	List<TuitionFeeView> getAllLoadedInvoices();

	ObservableList<TuitionFeeView> getTuitionFeesForCurrentUser();

	Object getTuitionFeeAmountForCurrentUser();

	TuitionFee addNewInvoice(TuitionFee newInvoice);

	void confirmInvoicePayment(TuitionFee fee);

	void sendInvoiceBackToStudent(TuitionFee fee);
}
