package hr.tvz.npupj.minireferada.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.tvz.npupj.minireferada.dao.SemestarRepository;
import hr.tvz.npupj.minireferada.model.Semestar;
import hr.tvz.npupj.minireferada.service.SemestarService;

@Service
public class SemestarServiceImpl implements SemestarService {

	@Autowired
	private SemestarRepository semestarRepository;

	@Override
	public List<Semestar> getAllSemestars() {
		return semestarRepository.findAll();
	}
}
