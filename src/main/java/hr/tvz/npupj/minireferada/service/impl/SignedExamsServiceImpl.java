package hr.tvz.npupj.minireferada.service.impl;

import hr.tvz.npupj.minireferada.dao.SignedExamsRepository;
import hr.tvz.npupj.minireferada.model.SignedExams;
import hr.tvz.npupj.minireferada.service.SignedExamsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mjovanovic on 29/05/15.
 */
@Service
public class SignedExamsServiceImpl implements SignedExamsService {

    @Autowired
    private SignedExamsRepository repository;

    @Override
    public List<SignedExams> getAllSignedExams() {
        return repository.findAll();
    }
}
