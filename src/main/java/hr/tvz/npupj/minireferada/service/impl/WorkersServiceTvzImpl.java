package hr.tvz.npupj.minireferada.service.impl;

import hr.tvz.npupj.minireferada.dao.WorkersTvzRepository;
import hr.tvz.npupj.minireferada.model.WorkersTvz;
import hr.tvz.npupj.minireferada.service.WorkersTvzService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WorkersServiceTvzImpl implements WorkersTvzService {
	
	@Autowired
	private WorkersTvzRepository repository;

	@Override
	public List<WorkersTvz> getAllWorkersTvz() {
	return repository.findAll();
	}

	@Override
	public WorkersTvz addNewWorker(WorkersTvz worker) {
	return repository.save(worker);
	}

}
