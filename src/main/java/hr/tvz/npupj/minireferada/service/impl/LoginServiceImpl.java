package hr.tvz.npupj.minireferada.service.impl;

import com.google.common.base.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import hr.tvz.npupj.minireferada.dao.PersonRepository;
import hr.tvz.npupj.minireferada.dao.StudentDataRepository;
import hr.tvz.npupj.minireferada.model.Osoba;
import hr.tvz.npupj.minireferada.model.Student;
import hr.tvz.npupj.minireferada.service.LoginService;

/**
 * Created by mjovanovic on 13/06/15.
 */
@Service
public class LoginServiceImpl implements LoginService {

    private static final Logger   LOGGER = LoggerFactory.getLogger(LoginServiceImpl.class);

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private StudentDataRepository studentDataRepository;
    
    @Override
    public Optional<Authentication> performeLogin(final String username, final String password) {

        Authentication authToken = null;

        try {
            authToken = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            SecurityContextHolder.getContext().setAuthentication(authToken);
        } catch (AuthenticationException e) {
            LOGGER.error("Fail to login user", e);
        }

        return Optional.fromNullable(authToken);
    }

	@Override
	public Osoba getUserData(String username) {
		return personRepository.getPersonByName(username);
	}
    
	@Override
	public Student getStudentData(Osoba user) {
		return studentDataRepository.findByUser(user);
	}
    
}
