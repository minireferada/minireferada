package hr.tvz.npupj.minireferada.service.impl;

import hr.tvz.npupj.minireferada.dao.KolegijRepository;
import hr.tvz.npupj.minireferada.model.Kolegij;
import hr.tvz.npupj.minireferada.model.Semestar;
import hr.tvz.npupj.minireferada.service.KolegijService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KolegijServiceImpl implements KolegijService {

	@Autowired
	private KolegijRepository kolegijRepository;

	@Override
	public List<Kolegij> getKolegijBySemestar(Semestar semestar) {
		return kolegijRepository.findKolegijBySemestar(semestar);
	}

}
