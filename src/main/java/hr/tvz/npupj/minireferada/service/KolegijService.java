package hr.tvz.npupj.minireferada.service;

import hr.tvz.npupj.minireferada.model.Kolegij;
import hr.tvz.npupj.minireferada.model.Semestar;

import java.util.List;

public interface KolegijService {
	
	public List<Kolegij> getKolegijBySemestar(Semestar semestar);

}
