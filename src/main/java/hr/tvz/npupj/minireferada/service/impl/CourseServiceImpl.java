package hr.tvz.npupj.minireferada.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.tvz.npupj.minireferada.dao.CourseRepository;
import hr.tvz.npupj.minireferada.model.Course;
import hr.tvz.npupj.minireferada.service.CourseService;

/**
 * Created by mjovanovic on 29/05/15.
 */
@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseRepository repository;

    @Override
    public List<Course> getAllCourses() {
        return repository.findAll();
    }

    @Override
    public Course addNewCourse(Course p_course) {
        return repository.save(p_course);
    }

    @Override
    public Course getCourseByName(final String p_name) {
        return repository.findByName(p_name);
    }
}
