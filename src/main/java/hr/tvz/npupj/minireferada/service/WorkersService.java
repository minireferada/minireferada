package hr.tvz.npupj.minireferada.service;

import hr.tvz.npupj.minireferada.model.WorkersRoles;

import java.util.List;

public interface WorkersService {
	public List<WorkersRoles> getAllWorkers();

}
