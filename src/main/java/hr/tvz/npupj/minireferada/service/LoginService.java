package hr.tvz.npupj.minireferada.service;

import hr.tvz.npupj.minireferada.model.Osoba;
import hr.tvz.npupj.minireferada.model.Student;

import com.google.common.base.Optional;

import org.springframework.security.core.Authentication;

/**
 * Created by mjovanovic on 13/06/15.
 */
public interface LoginService {

    Optional<Authentication> performeLogin(String username, String password);
    
    Osoba getUserData(String username);

	Student getStudentData(Osoba user);
}
