package hr.tvz.npupj.minireferada.service;

import hr.tvz.npupj.minireferada.model.Semestar;

import java.util.List;

public interface SemestarService {
	
	public List<Semestar> getAllSemestars();

}
