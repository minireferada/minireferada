package hr.tvz.npupj.minireferada.service;

import hr.tvz.npupj.minireferada.model.Exams;

import java.util.List;

/**
 * Created by mjovanovic on 29/05/15.
 */
public interface ExamsService {

    List<Exams> getAllExams();
}
