package hr.tvz.npupj.minireferada.service.impl;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;







import hr.tvz.npupj.minireferada.dao.StudentAdmissionDataRepository;


import hr.tvz.npupj.minireferada.model.StudentAdmission;
import hr.tvz.npupj.minireferada.service.StudentAdmissionService;

/**
 * Created by mjovanovic on 29/05/15.
 */
@Service
public class StudentAdmissionServiceImpl implements StudentAdmissionService {

    @Autowired
    private  StudentAdmissionDataRepository repository;

    @Override
    public List<StudentAdmission> getAllStudentAdmission() {
        return repository.findAll();
    }

    @Override
    public StudentAdmission getStudentAdmissionById(BigInteger id) {
        return repository.findById(id);
    }


}
