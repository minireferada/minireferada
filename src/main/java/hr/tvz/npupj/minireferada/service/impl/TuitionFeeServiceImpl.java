package hr.tvz.npupj.minireferada.service.impl;

import hr.tvz.npupj.minireferada.common.Constants;
import hr.tvz.npupj.minireferada.common.DataCollector;
import hr.tvz.npupj.minireferada.dao.PersonRepository;
import hr.tvz.npupj.minireferada.dao.StudentDataRepository;
import hr.tvz.npupj.minireferada.dao.TuitionFeeRepository;
import hr.tvz.npupj.minireferada.model.Osoba;
import hr.tvz.npupj.minireferada.model.Student;
import hr.tvz.npupj.minireferada.model.TuitionFee;
import hr.tvz.npupj.minireferada.model.TuitionFeeView;
import hr.tvz.npupj.minireferada.service.TuitionFeeService;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TuitionFeeServiceImpl implements TuitionFeeService {

	@Autowired
	private TuitionFeeRepository tuitionFeeRepository;

	@Autowired
	private PersonRepository personRepository;
	
	@Autowired
	private StudentDataRepository studentDataRepository;

	@Override
	public ObservableList<TuitionFeeView> getTuitionFeesForStudent(Student student) {
		List<TuitionFee> dbList = tuitionFeeRepository
				.getTuituionFessByStudent(student);
		List<TuitionFeeView> viewList = new ArrayList<TuitionFeeView>();
		for (TuitionFee tuitionFee : dbList) {
			viewList.add(new TuitionFeeView(tuitionFee));
		}
		return FXCollections.observableList(viewList);
	}

	@Override
	public Osoba getPersonForUsername(String username) {
		Osoba person = personRepository.getPersonByName(username);
		return person;
	}

	@Override
	public BigInteger getTuitionFeeAmountForPerson(Student student) {
		return tuitionFeeRepository.sumAmountByPerson(student);
	}

	@Override
	public TuitionFee getTuitionFeeById(Integer cellValue) {
		return tuitionFeeRepository.findOne(BigInteger.valueOf(cellValue));
	}

	@Override
	public TuitionFee setFilenameForPersonTuitionFee(String filename,
			BigInteger tuitionFeeID) {
		TuitionFee tf = tuitionFeeRepository.findOne(tuitionFeeID);
		tf.setFilePath(filename);
		tf.setStatus(Constants.TF_STATUS_PREDANO);
		return tuitionFeeRepository.save(tf);
	}

	@Override
	public List<TuitionFeeView> getAllLoadedInvoices() {
		List<TuitionFee> fees = tuitionFeeRepository.getTuitionFeesByStatus(Constants.TF_STATUS_PREDANO);
		List<TuitionFeeView> viewList = new ArrayList<TuitionFeeView>();
		for (TuitionFee tuitionFee : fees) {
			viewList.add(new TuitionFeeView(tuitionFee));
		}
		return FXCollections.observableList(viewList);
	}

	@Override
	public ObservableList<TuitionFeeView> getTuitionFeesForCurrentUser() {
		Student student = studentDataRepository.findByUser(DataCollector.getUser());
		return getTuitionFeesForStudent(student);
	}

	@Override
	public Object getTuitionFeeAmountForCurrentUser() {
		Student student = studentDataRepository.findByUser(DataCollector.getUser());
		return getTuitionFeeAmountForPerson(student);
	}

	@Override
	public TuitionFee addNewInvoice(TuitionFee newInvoice) {
		return tuitionFeeRepository.saveAndFlush(newInvoice);
	}

	@Override
	public void confirmInvoicePayment(TuitionFee fee) {
		fee.setStatus("POTVRDENO");
		tuitionFeeRepository.save(fee);
	}

	@Override
	public void sendInvoiceBackToStudent(TuitionFee fee) {
		fee.setStatus("VRACENO");
		tuitionFeeRepository.save(fee);
	}
	
}
