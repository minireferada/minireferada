package hr.tvz.npupj.minireferada.service;

import java.math.BigInteger;
import java.util.List;

import hr.tvz.npupj.minireferada.model.StudentAdmission;

/**
 * Created by mjovanovic on 29/05/15.
 */
public interface StudentAdmissionService {

    List<StudentAdmission> getAllStudentAdmission();

    StudentAdmission getStudentAdmissionById(BigInteger id);
}
