package hr.tvz.npupj.minireferada.util;

import hr.tvz.npupj.minireferada.annotation.FxModelAttribute;
import hr.tvz.npupj.minireferada.annotation.FxModelAttributes;
import hr.tvz.npupj.minireferada.config.SpringApplicationContextHolder;
import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import hr.tvz.npupj.minireferada.mapper.ValueMapper;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by mjovanovic on 26/05/15.
 */
public final class FxModelMapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(FxModelMapper.class);

    private FxModelMapper() {
    }

    public static void map(AbstractScreenController controller, Object model) {
        LOGGER.info("Mapping model '{}' at controller '{}'", model.getClass().getSimpleName(), controller.getClass().getSimpleName());

        List<Field> fields = FieldUtils.getAllFieldsList(model.getClass());
        for (Field field : fields) {

            List<FxModelAttributes> groupAnnotationsOnField = getAnnotationsFromField(field, FxModelAttributes.class);

            if (!groupAnnotationsOnField.isEmpty()) {

                LOGGER.debug("For field '{}' we have multiple mapping annotation", field.getName());

                for (FxModelAttributes fxModelAttributes : groupAnnotationsOnField) {
                    List<FxModelAttribute> annotationsOnField = Arrays.asList(fxModelAttributes.value());

                    processAttributeAnnotations(controller, model, field, annotationsOnField);
                }

            } else {
                List<FxModelAttribute> annotationsOnField = getAnnotationsFromField(field, FxModelAttribute.class);

                processAttributeAnnotations(controller, model, field, annotationsOnField);
            }

        }
    }

    private static void processAttributeAnnotations(AbstractScreenController controller, Object model, Field field, List<FxModelAttribute> annotationsOnField) {

        LOGGER.debug("For field '{}' total of {} mappings are found.", field.getName(), annotationsOnField.size());

        for (FxModelAttribute annotation : annotationsOnField) {
            Class<?> controllerFilter = annotation.controller();
            Class<?> targetFieldType = annotation.value();

            // We have controller filter
            if (!controllerFilter.equals(Void.class)) {
                if (!controller.getClass().equals(controllerFilter)) {
                    LOGGER.debug("Mapping for field '{}' is for controller '{}'. Skipping mapping as we process mapping for controller '{}'", field.getName(),
                            controllerFilter.getSimpleName(), controller.getClass().getSimpleName());
                    continue;
                }
            }

            mapValue(targetFieldType, field, controller, model);
        }
    }

    private static void mapValue(Class<?> targetFieldType, Field modelField, AbstractScreenController controller, Object model) {

        LOGGER.debug("Process mapping for field '{}' with type '{}' for controller '{}'. Searching for value mapper...", modelField.getName(), targetFieldType.getSimpleName(),
                controller.getClass().getSimpleName());

        ApplicationContext appCtx = SpringApplicationContextHolder.getAppCtx();
        Collection<ValueMapper> valueMappers = appCtx.getBeansOfType(ValueMapper.class).values();

        boolean mapperFound = false;
        for (ValueMapper valueMapper : valueMappers) {
            if (valueMapper.supports(targetFieldType)) {
                mapperFound = true;
                LOGGER.debug("Mapper found. Type '{}'", valueMapper.getClass().getSimpleName());

                valueMapper.map(model, modelField, controller);
            }
        }

        if (!mapperFound) {
            LOGGER.warn("Fail to find mapper for type '{}'", targetFieldType);
        }
    }

    private static <T extends Annotation> List<T> getAnnotationsFromField(Field field, Class<T> annotationType) {
        List annotationList = new ArrayList<>();
        Annotation[] annotations = field.getDeclaredAnnotations();

        for (Annotation annotation : annotations) {
            if (annotation.annotationType().equals(annotationType)) {
                annotationList.add(annotation);
            }
        }

        return annotationList;
    }
}
