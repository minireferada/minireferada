package hr.tvz.npupj.minireferada.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javafx.scene.control.DatePicker;

public class DateUtils {

	public static final String UI_DATE_FORMAT = "dd.MM.YYYY";	
	
	public static String formatUiDate(Date dueDate) {
		SimpleDateFormat format = new SimpleDateFormat(UI_DATE_FORMAT);
		return format.format(dueDate);
	}

	public static Date getDateFromDatePicker(DatePicker dp) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, dp.getValue().getDayOfMonth());
		cal.set(Calendar.MONTH, dp.getValue().getMonthValue());
		cal.set(Calendar.YEAR, dp.getValue().getYear());
		return cal.getTime();
	}

}
