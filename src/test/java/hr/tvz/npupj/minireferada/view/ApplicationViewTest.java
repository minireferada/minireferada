package hr.tvz.npupj.minireferada.view;

import org.junit.Test;
import org.testfx.api.FxAssert;
import org.testfx.matcher.base.NodeMatchers;

public class ApplicationViewTest extends AbstractSpringFxTest {

	@Override
	protected String screenToLoad() {
		return "loginScreen";
	}
	
	@Test
	public void testLogout() {
		clickOn("#txtUsername").write("referada");
        clickOn("#txtPassword").write("referada");
        clickOn("#btnLogin");
        FxAssert.verifyThat("MiniReferada Referada", NodeMatchers.isNotNull());
        clickOn("Logout");
        FxAssert.verifyThat("Prijava", NodeMatchers.isNotNull());
	}
	
	@Test
	public void testSwitchUsers(){
		clickOn("#txtUsername").write("s");
        clickOn("#txtPassword").write("s");
        clickOn("#btnLogin");
        FxAssert.verifyThat("MiniReferada - Student", NodeMatchers.isNotNull());
        clickOn("#menuLogout");
        clickOn("#menuItemLogout");
//        clickOn("#txtUsername").write("referada");
//        clickOn("#txtPassword").write("referada");
//        clickOn("#btnLogin");
//        FxAssert.verifyThat("MiniReferada Referada", NodeMatchers.isNotNull());
	}
}
