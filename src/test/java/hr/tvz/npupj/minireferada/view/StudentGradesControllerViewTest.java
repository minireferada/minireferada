package hr.tvz.npupj.minireferada.view;

import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.base.NodeMatchers.isNotNull;

import org.junit.Test;

public class StudentGradesControllerViewTest extends AbstractSpringFxTest {

	@Override
	protected String screenToLoad() {
		return "studentGradesScreen";
	}

	@Test
	public void loginNok() {
		clickOn("#cmbOdabirSemestra");
		verifyThat("#cmbOdabirSemestra", isNotNull());
	}

	@Test
	public void isKolegijListEmpty() {
		clickOn("#cmbOdabirSemestra").clickOn("1. SEMESTAR").clickOn("#cmbOdabirSemestra").clickOn("2. SEMESTAR")
		.clickOn("#cmbOdabirSemestra").clickOn("3. SEMESTAR").clickOn("#cmbOdabirSemestra").clickOn("4. SEMESTAR");
	}

}
