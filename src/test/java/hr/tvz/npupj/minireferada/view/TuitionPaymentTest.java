package hr.tvz.npupj.minireferada.view;

import hr.tvz.npupj.minireferada.common.DataCollector;

import org.junit.Test;
import org.testfx.api.FxAssert;
import org.testfx.matcher.base.NodeMatchers;

public class TuitionPaymentTest extends AbstractSpringFxTest {
	
	@Override
	protected String screenToLoad() {
		return "loginScreen";
	}
	
	public void loginStudentOpenTuition() {
		clickOn("#txtUsername").write("s");
        clickOn("#txtPassword").write("s");
        clickOn("#btnLogin");
        clickOn("#menuTuitionFee");
        clickOn("#menuItemTuitionFee");
	}
	
	public void loginReferada() {
		clickOn("#txtUsername").write("referada");
        clickOn("#txtPassword").write("referada");
        clickOn("#btnLogin");
	}
	
	@Test
	public void testAddInvoice(){
		loginReferada();
		clickOn("#tabUplate");
		clickOn("#btnNewInvoice");
		FxAssert.verifyThat("DODAVANJE UPLATNICE", NodeMatchers.isNotNull());
		clickOn("Dodaj");
		
		FxAssert.verifyThat("Informacija", NodeMatchers.isNotNull());
		FxAssert.verifyThat("#message", NodeMatchers.isNotNull());
	}
	
	public void testAddInvoiceComplete() {
		loginReferada();
		clickOn("#tabUplate");
		clickOn("#btnNewInvoice");
		FxAssert.verifyThat("DODAVANJE UPLATNICE", NodeMatchers.isNotNull());
		clickOn("#feeDesc").write("Opis");
		clickOn("#feeStatus").clickOn("PREDANO");
		clickOn("#feeAmount").write("100");
		clickOn("Dodaj");
		
		FxAssert.verifyThat("Informacija", NodeMatchers.isNotNull());
		FxAssert.verifyThat("#message", NodeMatchers.isNotNull());
		closeCurrentWindow();
		
		clickOn("#feeType").clickOn("SKOLARINA");
		clickOn("#feeStudent").clickOn("Pero Perić");
		clickOn("#feeDueDate").write("03.07.2015.");
		clickOn("Dodaj");
		closeCurrentWindow();
	}
	
	@Test
	public void testConfirmingPayment() {
		loginReferada();
        clickOn("#tabUplate");
        clickOn("Uplatnica");
        clickOn("#btnConfirm");
        FxAssert.verifyThat("#tbLoadedInvoice", NodeMatchers.hasChildren(0, "PREDANO"));
        closeCurrentWindow();
        clickOn("Logout");
	}
	
	
	
	@Test
	public void testGeneratingInvoice() {
		loginStudentOpenTuition();
		clickOn("Uplatnica");
		FxAssert.verifyThat("Uplatnica", NodeMatchers.isNotNull());
		FxAssert.verifyThat("Tehničko veleučilište u Zagrebu", NodeMatchers.isNotNull());
		FxAssert.verifyThat(DataCollector.getStudentData().getFirstname(), NodeMatchers.isNotNull());
		FxAssert.verifyThat(DataCollector.getStudentData().getLastname(), NodeMatchers.isNotNull());
		FxAssert.verifyThat("02032939393", NodeMatchers.isNotNull());
		FxAssert.verifyThat("322324242424242", NodeMatchers.isNotNull());
		FxAssert.verifyThat("ŠKOLARINA ZA 2. SEMESTAR", NodeMatchers.isNotNull());
	}
	
	@Test
	public void testLoadingInvoice() {
		loginStudentOpenTuition();
		clickOn("Plaćanje");
		FxAssert.verifyThat("Učitavanje uplatnice", NodeMatchers.isNotNull());
		clickOn("#btnLoadInvoice");
		closeCurrentWindow();
		FxAssert.verifyThat("#tuitionsFeeTable", NodeMatchers.hasChildren(0, "PREDANO"));
	}
	
	@Test
	public void testTuitionFeeScreenAccountStatus() {
		loginStudentOpenTuition();
        FxAssert.verifyThat("#accountStatus", NodeMatchers.hasText("-3950"));
	}

	@Test
	public void testTuitionFeeScreenFeesNumber() {
		loginStudentOpenTuition();
        FxAssert.verifyThat("#tuitionsFeeTable", NodeMatchers.hasChildren(4, "Uplatnica"));
	}
	
	
	
	
}
