package hr.tvz.npupj.minireferada.view;

import org.junit.Test;

import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.base.NodeMatchers.hasText;
import static org.testfx.matcher.base.NodeMatchers.isNotNull;

/**
 * Created by mjovanovic on 02/07/15.
 */
public class LoginControllerViewTest extends AbstractSpringFxTest {

    @Override
    protected String screenToLoad() {
        return "loginScreen";
    }

    @Test
    public void loginNok() {
        // given:
        clickOn("#txtUsername").write("1234");
        clickOn("#txtPassword").write("1234");

        // when:
        clickOn("#btnLogin");

        // then:
        verifyThat("#lblMessage", hasText("Unjeli ste krive podatke!"));
    }

    @Test
    public void loginStudentOk() {
        // given:
        clickOn("#txtUsername").write("s");
        clickOn("#txtPassword").write("s");

        // when:
        clickOn("#btnLogin");

        // then:
        verifyThat("#menuBar", isNotNull());
    }

    @Test
    public void loginReferadaOk() {
        // given:
        clickOn("#txtUsername").write("referada");
        clickOn("#txtPassword").write("referada");

        // when:
        clickOn("#btnLogin");

        // then:
        verifyThat("#StudentTabSadrzaj", isNotNull());
    }
}
