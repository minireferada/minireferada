package hr.tvz.npupj.minireferada.view;

import org.junit.Test;

import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.base.NodeMatchers.hasText;
import static org.testfx.matcher.base.NodeMatchers.isNotNull;


public class AddNewStudentTest extends AbstractSpringFxTest {

    @Override
    protected String screenToLoad() {
        return "loginScreen";
    }

    
    @Test
    public void addNewStudent() {
        // given:
    	// LOGIN U REFERADU
        clickOn("#txtUsername").write("referada");
        clickOn("#txtPassword").write("referada");

        // when:
        clickOn("#btnLogin");

        // then:
        verifyThat("#StudentTabSadrzaj", isNotNull()); 
        
        
        // TU IDE TAB UNOS STUDENTA
        clickOn("#tabUnosStudenta");
        
        // TU ODABIRE EKRAN ZA UNOS NOVOG STUDENTA
        clickOn("#btnUnosStudenta");
        
        // SAD JE NA EKRANU ZA UNOS NOVOG STUDENTA
        
        clickOn("#txtStudentName").write("Ivo");
        clickOn("#txtStudentLastname").write("Ivić");
        clickOn("#txtStudentBirthDate").write("20-01-1988");
        clickOn("#txtStudentAdress").write("Moja ulica 1a");
        clickOn("#txtStudentCity").write("Velik Grad");
        clickOn("#txtStudentPostNumber").write("38 000");
        clickOn("#txtStudentOib").write("2347656786");
        clickOn("#txtStudentJmbag").write("12345678991");
        clickOn("#txtStudentEmail").write("neki@mail.com");
        clickOn("#txtStudentPhone").write("555-555-555");
        
        clickOn("#txtUserName").write("Ivo1");
        clickOn("#txtPassword").write("PassIvo1");
        

        // when:
        clickOn("#btnSaveStudent");
        
        // when:
        clickOn("#btnLogout");
        

    }

    @Test
    public void LoginNewStudent() {
        // given:
    	// LOGIN U REFERADU
        clickOn("#txtUsername").write("Ivo1");
        clickOn("#txtPassword").write("PassIvo1");

        // when:
        clickOn("#btnLogin");

        // then:
        verifyThat("#StudentTabSadrzaj", isNotNull()); 
        
        
        // TU IDE TAB UNOS STUDENTA
        clickOn("#tabUnosStudenta");


    }
    
    @Test
    public void DeleteNewStudent() {
        // given:
    	// LOGIN U REFERADU
        clickOn("#txtUsername").write("referada");
        clickOn("#txtPassword").write("referada");

        // when:
        clickOn("#btnLogin");

        // then:
        verifyThat("#StudentTabSadrzaj", isNotNull()); 
        
        
        // TU IDE TAB UNOS STUDENTA
        clickOn("#tabUnosStudenta");

        // TU ODABIRE EKRAN ZA UNOS NOVOG STUDENTA
        clickOn("#btnDeleteStudenta");

        clickOn("#txtFindByJMBAG").write("12345678991");
        
        // when:
        clickOn("#btnEnterJmbagStudent");
        
        // when:
        clickOn("#btnDeleteStudent");
        
     // when:
        clickOn("#txtFindByJMBAG").write("12345678991");

        clickOn("#btnEnterJmbagStudent");
        
        // then:
        verifyThat("#txtFindByJMBAG", hasText("KRIVI JMBAG"));
    }
    
    @Test
    public void TestDeletedStudentLogin() {
        // given:
    	// LOGIN U REFERADU
        clickOn("#txtUsername").write("Ivo1");
        clickOn("#txtPassword").write("PassIvo11");

        // when:
        clickOn("#btnLogin");

        // then:
        verifyThat("#lblMessage", hasText("Unjeli ste krive podatke!"));

    }
}
