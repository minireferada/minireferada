package hr.tvz.npupj.minireferada.view;

import javafx.stage.Stage;

import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.testfx.framework.junit.ApplicationTest;

import hr.tvz.npupj.minireferada.ViewTestSpringConfiguration;
import hr.tvz.npupj.minireferada.config.FxmlScreen;

/**
 * Created by mjovanovic on 02/07/15.
 */
@RunWith(BlockJUnit4ClassRunner.class)
public abstract class AbstractSpringFxTest extends ApplicationTest {

    private static AnnotationConfigApplicationContext appCtx;

    @Override
    public void start(final Stage p_stage) throws Exception {
        if (appCtx == null) {
            appCtx = new AnnotationConfigApplicationContext();
            appCtx.register(ViewTestSpringConfiguration.class);
            appCtx.refresh();
        }

        FxmlScreen screen = appCtx.getBean(screenToLoad(), FxmlScreen.class);
        p_stage.setScene(screen.getScene());
        p_stage.show();
    }

    protected abstract String screenToLoad();
}
