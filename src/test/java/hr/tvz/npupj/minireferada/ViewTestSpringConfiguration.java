package hr.tvz.npupj.minireferada;

import hr.tvz.npupj.minireferada.config.SpringApplicationContextHolder;
import hr.tvz.npupj.minireferada.config.SpringConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/**
 * Created by mjovanovic on 20/06/15.
 */
@EnableJpaRepositories("hr.tvz.npupj.minireferada.dao")
@ComponentScan(value = "hr.tvz.npupj.minireferada", excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = SpringConfiguration.class))
@Configuration
public class ViewTestSpringConfiguration {

    @Bean
    public SpringApplicationContextHolder applicationContextHolder(ApplicationContext appCtx) {
        return new SpringApplicationContextHolder(appCtx);
    }

    // Hibernate config

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    // Hibernate config

    @Bean
    public PlatformTransactionManager transactionManager(DataSource dataSource) {
        EntityManagerFactory factory = entityManagerFactory(dataSource).getObject();
        return new JpaTransactionManager(factory);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setGenerateDdl(false);
        adapter.setShowSql(true);

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setDataSource(dataSource);
        factory.setJpaVendorAdapter(adapter);
        factory.setPackagesToScan("hr.tvz.npupj.minireferada.model");
        factory.afterPropertiesSet();

        return factory;
    }

    @Bean
    public DataSource dataSource() {
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        builder.setType(EmbeddedDatabaseType.H2);
        builder.addScript("/sql/database.ddl");
        builder.addScript("/sql/import.sql");
        return builder.build();

    }

}
