package hr.tvz.npupj.minireferada.integration;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javafx.embed.swing.JFXPanel;

import javax.swing.*;

import org.junit.BeforeClass;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import hr.tvz.npupj.minireferada.IntegrationTestSpringConfiguration;

/**
 * Created by mjovanovic on 02/07/15.
 */
@ContextConfiguration(classes = IntegrationTestSpringConfiguration.class)
@DirtiesContext
public abstract class AbstractIntegrationTest extends AbstractTransactionalJUnit4SpringContextTests {

    @BeforeClass
    public static void initToolkit() throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);
        SwingUtilities.invokeLater(() -> {
            new JFXPanel(); // initializes JavaFX environment
                latch.countDown();
            });

        // That's a pretty reasonable delay... Right?
        if (!latch.await(5L, TimeUnit.SECONDS))
            throw new ExceptionInInitializerError();
    }
}
