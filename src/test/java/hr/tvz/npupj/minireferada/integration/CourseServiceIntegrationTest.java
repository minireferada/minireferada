package hr.tvz.npupj.minireferada.integration;

import hr.tvz.npupj.minireferada.model.Course;
import hr.tvz.npupj.minireferada.service.CourseService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by mjovanovic on 02/07/15.
 */
public class CourseServiceIntegrationTest extends AbstractIntegrationTest {

    private static final int    SCRIPT_COURSE_LIST_SIZE = 76;
    private static final String MOCK_COURSE_NAME        = "MockTestCourse";

    @Autowired
    private CourseService       courseService;

    @Test
    public void readAllTest() {
        List<Course> allCourses = courseService.getAllCourses();

        assertEquals(allCourses.size(), SCRIPT_COURSE_LIST_SIZE);
    }

    @Test
    public void insertTest() {
        Course course = new Course();
        course.setName(MOCK_COURSE_NAME);

        Course persistedCourse = courseService.addNewCourse(course);
        assertNotNull(persistedCourse.getId());

        Course foundedCourse = courseService.getCourseByName(MOCK_COURSE_NAME);
        assertEquals(foundedCourse.getId(), persistedCourse.getId());

        List<Course> allCourses = courseService.getAllCourses();
        assertEquals(allCourses.size(), SCRIPT_COURSE_LIST_SIZE + 1);
    }
}
