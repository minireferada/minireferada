package hr.tvz.npupj.minireferada.integration.util;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import org.junit.Before;
import org.junit.Test;

import hr.tvz.npupj.minireferada.annotation.FxModelAttribute;
import hr.tvz.npupj.minireferada.annotation.FxModelAttributes;
import hr.tvz.npupj.minireferada.controller.common.AbstractScreenController;
import hr.tvz.npupj.minireferada.integration.AbstractIntegrationTest;
import hr.tvz.npupj.minireferada.util.FxModelMapper;
import junit.framework.Assert;

/**
 * Created by mjovanovic on 04/07/15.
 */
public class FxModelMapperTest extends AbstractIntegrationTest {

    private static final String MOCK_TEXT         = "mock";
    private static final String MOCK_TEXT_LABEL   = "mockLabel";
    private static final String MOCK_TEXT_TEXTBOX = "mockText";

    private TestControllerClass controller;

    @Before
    public void setup() {
        controller = new TestControllerClass();
    }

    @Test
    public void mappingOkTest() {
        TestDtoClass dto = new TestDtoClass(MOCK_TEXT_LABEL, MOCK_TEXT_TEXTBOX, MOCK_TEXT);

        FxModelMapper.map(controller, dto);

        Assert.assertEquals(controller.getLblMockLabel().getText(), MOCK_TEXT_LABEL);
        Assert.assertEquals(controller.getTxtMockText().getText(), MOCK_TEXT_TEXTBOX);

        Assert.assertEquals(controller.getLblMock().getText(), MOCK_TEXT);
        Assert.assertEquals(controller.getTxtMock().getText(), MOCK_TEXT);
    }

    public void mappingNokTest() {

    }

    private final class TestControllerClass extends AbstractScreenController {

        private Label     lblMockLabel;
        private TextField txtMockText;

        private Label     lblMock;
        private TextField txtMock;

        public TestControllerClass() {
            this.lblMockLabel = new Label();
            this.txtMockText = new TextField();

            this.lblMock = new Label();
            this.txtMock = new TextField();
        }

        public Label getLblMockLabel() {
            return lblMockLabel;
        }

        public void setLblMockLabel(final Label p_lblMockLabel) {
            lblMockLabel = p_lblMockLabel;
        }

        public TextField getTxtMockText() {
            return txtMockText;
        }

        public void setTxtMockText(final TextField p_txtMockText) {
            txtMockText = p_txtMockText;
        }

        public Label getLblMock() {
            return lblMock;
        }

        public void setLblMock(final Label p_lblMock) {
            lblMock = p_lblMock;
        }

        public TextField getTxtMock() {
            return txtMock;
        }

        public void setTxtMock(final TextField p_txtMock) {
            txtMock = p_txtMock;
        }
    }

    private final class TestDtoClass {

        @FxModelAttribute(Label.class)
        private String mockLabel;
        @FxModelAttribute(TextField.class)
        private String mockText;

        @FxModelAttributes({
                @FxModelAttribute(Label.class), @FxModelAttribute(TextField.class)
        })
        private String mock;

        public TestDtoClass(String p_mockLabel, String p_mockText, String p_mock) {
            this.mockLabel = p_mockLabel;
            this.mockText = p_mockText;
            this.mock = p_mock;
        }

        public String getMockLabel() {
            return mockLabel;
        }

        public void setMockLabel(final String p_mockLabel) {
            mockLabel = p_mockLabel;
        }

        public String getMockText() {
            return mockText;
        }

        public void setMockText(final String p_mockText) {
            mockText = p_mockText;
        }

        public String getMock() {
            return mock;
        }

        public void setMock(final String p_mock) {
            mock = p_mock;
        }
    }
}
