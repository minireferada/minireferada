package hr.tvz.npupj.minireferada.integration;

import hr.tvz.npupj.minireferada.model.Kolegij;
import hr.tvz.npupj.minireferada.model.Semestar;
import hr.tvz.npupj.minireferada.service.KolegijService;
import hr.tvz.npupj.minireferada.service.SemestarService;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class SemestarServiceIntegrationTest extends AbstractIntegrationTest {

    private static final int SEMESTARS_SIZE = 12;

    @Autowired
    private SemestarService  semestarService;

    @Autowired
    private KolegijService   kolegijService;

    @Test
    public void readAllSemestars() {

        List<Semestar> allSemestars = semestarService.getAllSemestars();

        assertEquals(SEMESTARS_SIZE, allSemestars.size());
    }

    /**
     * test za provjeru je li lista kolegija prazna
     */
    @Test
    public void readSemestarsByKolegij() {

        List<Semestar> allSemestars = semestarService.getAllSemestars();

        for (Semestar s : allSemestars) {
            List<Kolegij> kolegij = kolegijService.getKolegijBySemestar(s);
            Assert.assertFalse(allSemestars.isEmpty());
        }
    }

    // @Test
    // public void readKolegijBySemestar(){
    //
    //
    // }

}
