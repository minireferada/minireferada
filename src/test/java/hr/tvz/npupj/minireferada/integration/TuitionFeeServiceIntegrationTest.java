package hr.tvz.npupj.minireferada.integration;

import hr.tvz.npupj.minireferada.dao.StudentDataRepository;
import hr.tvz.npupj.minireferada.model.Student;
import hr.tvz.npupj.minireferada.model.TuitionFee;
import hr.tvz.npupj.minireferada.model.TuitionFeeView;
import hr.tvz.npupj.minireferada.service.TuitionFeeService;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TuitionFeeServiceIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private TuitionFeeService tuitionFeeService;
    @Autowired
    private StudentDataRepository studentDataRepository;

    BigInteger dbAmount = BigInteger.valueOf(-4150);
    String stJMBAG = "98546783223";
    // 1. test fees sum
    @Test
    public void testStudentTuitionFeeAmount() {
    	Student student = studentDataRepository.findByJmbag(stJMBAG);
        BigInteger amount = tuitionFeeService.getTuitionFeeAmountForPerson(student);
        if (!amount.equals(dbAmount)) {
        	Assert.fail("Pogrešno računanje iznosa školarine");
        }
    }

    // 2. test sending invoice
    @Test
    public void testSendingInvoice() {
        String filepath = "D:/path/path/image.jpg";
        BigInteger ID = BigInteger.valueOf(1);
        TuitionFee tf = tuitionFeeService.setFilenameForPersonTuitionFee(filepath, ID);
        if (!tf.getStatus().equals("PREDANO") || tf.getFilePath() == null || tf.getFilePath().isEmpty()) {
            Assert.fail("Pogreška prilikom slanja uplatnice!");
        }
    }

    // 3. test reviewing invoice
    @Test
    public void testConfirmInvoice() {
    	Student student = studentDataRepository.findByJmbag(stJMBAG);
    	List<TuitionFeeView> loadedInvoices = tuitionFeeService.getAllLoadedInvoices();
    	TuitionFeeView tfv = loadedInvoices.get(0);
    	TuitionFee tf = tuitionFeeService.getTuitionFeeById(tfv.getId());
    	tuitionFeeService.confirmInvoicePayment(tf);
    	if (!tf.getStatus().equals("POTVRDENO") || tf.getFilePath() == null) {
    		Assert.fail("Greška prilikom potvrđivanja uplate");
    	}
    	BigInteger newAmount = dbAmount.add(BigInteger.valueOf(tf.getFeeAmount().intValue()));
    	BigInteger calcAmount = tuitionFeeService.getTuitionFeeAmountForPerson(student);
    	if (!newAmount.equals(calcAmount)) {
    		Assert.fail("Pogreška prilikom računanja sume nakon potvrđivanja uplatnice");
    	}
    }
    // 4. test fetching invoice
    @Test
    public void testFetchingInvoiceForStudent() {
    	Student student = studentDataRepository.findByJmbag(stJMBAG);
    	List<TuitionFeeView> stTuitionFees = tuitionFeeService.getTuitionFeesForStudent(student);
    	if (stTuitionFees.size() != 4) {
    		Assert.fail("Pogrešan dohvat broja troškova studenta");
    	}
    }
    // 5.test adding invoice
    @Test
    public void testAddingInvoice() {
    	Student student = studentDataRepository.findByJmbag(stJMBAG);
    	TuitionFee newtf = new TuitionFee();
    	newtf.setDescription("Test");
    	newtf.setDueDate(new Date());
    	newtf.setFeeAmount(BigDecimal.ONE);
    	newtf.setPaidAmount(BigDecimal.ZERO);
    	newtf.setStatus("DODJELA");
    	newtf.setTuitionType("TROSKOVI");
    	newtf.setStudent(student);
    	TuitionFee savedtf = tuitionFeeService.addNewInvoice(newtf);
    	if (!newtf.equals(savedtf)) {
    		Assert.fail("Neispravni podaci spremljeni u bazu prilikom dodavanja novog troška");
    	}
    }
    // 6.test return invoice to student
    @Test
    public void testReturnInvoiceBackToStudent() {
    	Student student = studentDataRepository.findByJmbag(stJMBAG);
    	List<TuitionFeeView> fees = tuitionFeeService.getTuitionFeesForStudent(student);
    	TuitionFee tf = tuitionFeeService.getTuitionFeeById(fees.get(0).getId());
    	tuitionFeeService.sendInvoiceBackToStudent(tf);
    	if(!tf.getStatus().equals("VRACENO")) {
    		Assert.fail("Neispravno postavljanje statusa kod vraćanja uplatnice studentu");
    	}
    }
}
