package hr.tvz.npupj.minireferada.integration;

import com.google.common.base.Optional;
import hr.tvz.npupj.minireferada.config.UserRole;
import hr.tvz.npupj.minireferada.service.LoginService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by mjovanovic on 02/07/15.
 */
public class LoginServiceIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private LoginService loginService;

    @Test
    public void loginStudentOk() {
        Optional<Authentication> authenticationOptional = loginService.performeLogin("s", "s");
        assertSuccessfulLoginAndRole(authenticationOptional, UserRole.ROLE_STUDENT);
    }

    @Test
    public void loginReferadaOk() {
        Optional<Authentication> authenticationOptional = loginService.performeLogin("referada", "referada");
        assertSuccessfulLoginAndRole(authenticationOptional, UserRole.ROLE_REFERADA);
    }

    private void assertSuccessfulLoginAndRole(Optional<Authentication> p_authenticationOptional, String p_userRole) {
        assertEquals(p_authenticationOptional.isPresent(), true);

        Authentication authentication = p_authenticationOptional.get();
        Collection<? extends GrantedAuthority> roles = authentication.getAuthorities();

        assertEquals(roles.size(), 1);

        SimpleGrantedAuthority studentRole = new SimpleGrantedAuthority(p_userRole);
        assertEquals(roles.contains(studentRole), true);
    }

}
