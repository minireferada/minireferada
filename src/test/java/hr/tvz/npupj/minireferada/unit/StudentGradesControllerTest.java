package hr.tvz.npupj.minireferada.unit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import hr.tvz.npupj.minireferada.controller.student.StudentGradesController;
import hr.tvz.npupj.minireferada.model.Kolegij;
import hr.tvz.npupj.minireferada.model.Semestar;
import hr.tvz.npupj.minireferada.service.SemestarService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;

import org.junit.Before;
import org.junit.Test;

public class StudentGradesControllerTest extends AbstractUnitTest {

	private StudentGradesController controller;

	private SemestarService semestarServiceMock;

	private ComboBox<Semestar> comboMock;

	@Before
	public void setUp() {

		controller = new StudentGradesController();
		comboMock = new ComboBox<Semestar>();
	}

	@Test
	public void isComboBoxElementOk() {

		ObservableList<Semestar> semestri = FXCollections.observableArrayList();
		semestri.add(new Semestar("1. semestar"));
		semestri.add(new Semestar("2. semestar"));
		semestri.add(new Semestar("3. semestar"));
		semestri.add(new Semestar("4. semestar"));
		semestri.add(new Semestar("5. semestar"));
		semestri.add(new Semestar("6. semestar"));

		

		comboMock.setItems(semestri);

		assertEquals("[1. semestar, 2. semestar, 3. semestar, 4. semestar, 5. semestar, 6. semestar]", comboMock.getItems()
				.toString());

	}

//	@Test
//	public void selectedSemestarGivesRightCourse() {
//
//
//	}

}
