package hr.tvz.npupj.minireferada.unit;

import com.google.common.base.Optional;
import hr.tvz.npupj.minireferada.controller.common.LoginController;
import hr.tvz.npupj.minireferada.service.LoginService;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.test.util.ReflectionTestUtils;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by mjovanovic on 02/07/15.
 */
public class LoginControllerTest extends AbstractUnitTest {

    private LoginController controller;

    private LoginService    loginServiceMock;

    private Label           lblMessageMock;
    private TextField       txtUsernameMock;
    private PasswordField   txtPasswordMock;

    @Before
    public void setup() {
        controller = new LoginController();

        loginServiceMock = mock(LoginService.class);

        lblMessageMock = new Label();
        txtUsernameMock = new TextField();
        txtPasswordMock = new PasswordField();

        ReflectionTestUtils.setField(controller, "lblMessage", lblMessageMock);
        ReflectionTestUtils.setField(controller, "txtUsername", txtUsernameMock);
        ReflectionTestUtils.setField(controller, "txtPassword", txtPasswordMock);
        ReflectionTestUtils.setField(controller, "loginService", loginServiceMock);

        when(loginServiceMock.performeLogin(eq("fail"), eq("fail"))).thenReturn(Optional.absent());
        when(loginServiceMock.performeLogin(eq("ok"), eq("ok"))).thenReturn(Optional.of(new UsernamePasswordAuthenticationToken(null, null)));
    }

    @Test
    public void loginNokTest() {
        txtUsernameMock.setText("fail");
        txtPasswordMock.setText("fail");

        controller.btnLoginAction(null);

        verify(loginServiceMock, times(0)).performeLogin(eq("ok"), eq("ok"));
        verify(loginServiceMock, times(1)).performeLogin(eq("fail"), eq("fail"));
        assertEquals(lblMessageMock.getText(), "Unjeli ste krive podatke!");
    }

    @Test
    public void loginOkTest() {
        txtUsernameMock.setText("ok");
        txtPasswordMock.setText("ok");

        controller.btnLoginAction(null);

        verify(loginServiceMock, times(1)).performeLogin(eq("ok"), eq("ok"));
        verify(loginServiceMock, times(0)).performeLogin(eq("fail"), eq("fail"));
        assertEquals(lblMessageMock.getText(), "");
    }
}
